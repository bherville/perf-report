source 'https://rubygems.org'

gem 'rails', '3.2.13'
gem 'pg'


# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
end

# UI
gem 'jquery-rails'
gem 'jquery-datatables-rails', :git => 'https://github.com/rweng/jquery-datatables-rails.git'
gem 'jquery-ui-rails'
gem 'will_paginate'
gem 'jbuilder'
gem 'execjs'
gem 'libv8'
gem 'spinjs-rails'

# Deployment
group :deployment do
  gem 'capistrano', '~> 3.2.0'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-rvm'
end

gem 'paperclip', '~> 4.2'

# Job Processing
gem 'sidekiq'
gem 'sidekiq-status'
gem 'clockwork'
gem 'daemons'
gem 'sinatra', '>= 1.3.0', :require => nil
gem 'sprockets'

# File Processing
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'rubyzip', '>= 1.0.0'

# Authentication
gem 'devise'
gem 'devise-encryptable'
gem 'devise-async'
gem 'cancan'
gem 'role_model'
gem 'devise_security_extension', '0.7.2'
gem 'rails_email_validator'
gem 'strong_parameters'

gem 'rest-client'

# InfluxDB Graphing
group :influxdb do
  gem 'influxdb'
  gem 'gruff'
end

gem 'omniauth-authman', :git => 'git@bitbucket.org:bherring/omniauth-authman.git', :branch => 'master'
gem 'look_and_feel', :git => 'https://bitbucket.org/bherring/look-and-feel-gem'
gem 'gravtastic', :git => 'https://bitbucket.org/bherring/gravtastic', :branch => 'master'

gem 'font-awesome-rails'

group :graph_source_modules do
  gem 'perf_report_nagios', :git => 'https://bitbucket.org/bherring/perf-report-nagios-gem', :branch => 'development'
end

gem 'audited-activerecord', '~> 3.0'