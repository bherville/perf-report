# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150803135419) do

  create_table "application_families", :force => true do |t|
    t.string   "name"
    t.boolean  "ignore_active"
    t.boolean  "active"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "application_family_applications", :force => true do |t|
    t.boolean  "ignore_active"
    t.boolean  "active"
    t.integer  "application_id"
    t.integer  "application_family_id"
    t.integer  "user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "applications", :force => true do |t|
    t.string   "name"
    t.string   "app_type"
    t.boolean  "major_application"
    t.boolean  "ignore_active"
    t.boolean  "active"
    t.integer  "user_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "graph_commands", :force => true do |t|
    t.string   "name"
    t.string   "command"
    t.integer  "user_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.boolean  "has_dates"
    t.integer  "graph_source_id"
  end

  create_table "graph_sources", :force => true do |t|
    t.string   "name"
    t.boolean  "default",                        :default => false, :null => false
    t.integer  "user_id"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.string   "graph_source_module"
    t.text     "graph_source_module_parameters"
  end

  create_table "graph_type_reports", :force => true do |t|
    t.integer  "graph_type_id"
    t.integer  "report_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "graph_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "user_id"
    t.integer  "display_order"
  end

  create_table "graphs", :force => true do |t|
    t.string   "name"
    t.string   "file_path"
    t.integer  "server_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "graph_type_id"
  end

  add_index "graphs", ["name", "server_id"], :name => "server_graphs"

  create_table "old_passwords", :force => true do |t|
    t.string   "encrypted_password",       :null => false
    t.string   "password_salt"
    t.string   "password_archivable_type", :null => false
    t.integer  "password_archivable_id",   :null => false
    t.datetime "created_at"
  end

  add_index "old_passwords", ["password_archivable_type", "password_archivable_id"], :name => "index_password_archivable"

  create_table "report_application_families", :force => true do |t|
    t.integer  "application_family_id"
    t.integer  "report_id"
    t.integer  "user_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "report_applications", :force => true do |t|
    t.integer  "report_id"
    t.integer  "application_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "report_file_emails", :force => true do |t|
    t.integer  "report_file_id"
    t.text     "email_addresses"
    t.boolean  "mailing_list_send", :default => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  create_table "report_files", :force => true do |t|
    t.integer  "report_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "job_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "user_id"
  end

  create_table "report_graphs", :force => true do |t|
    t.integer  "report_id"
    t.string   "graph_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "report_servers", :force => true do |t|
    t.integer  "server_id"
    t.integer  "report_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reports", :force => true do |t|
    t.string   "name"
    t.datetime "send_time"
    t.text     "mailing_list"
    t.integer  "user_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "send_day"
    t.string   "graph_command"
    t.datetime "last_time_sent"
    t.text     "report_title"
    t.boolean  "schedule_report_run",             :default => true
    t.integer  "graph_command_id"
    t.boolean  "public",                          :default => false
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "graph_source_id"
    t.boolean  "include_application_information", :default => false
    t.boolean  "show_all_applications",           :default => true
    t.string   "time_zone"
    t.string   "image_type"
  end

  create_table "server_applications", :force => true do |t|
    t.text     "description"
    t.boolean  "ignore_active"
    t.boolean  "active"
    t.integer  "application_id"
    t.integer  "server_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "server_group_profiles", :force => true do |t|
    t.integer  "server_id"
    t.integer  "server_group_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "server_group_reports", :force => true do |t|
    t.integer  "server_group_id"
    t.integer  "report_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "server_group_user_profiles", :force => true do |t|
    t.integer  "server_group_id"
    t.integer  "user_profile_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "server_groups", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "servers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "servers", ["name"], :name => "index_servers_on_name"

  create_table "shared_reports", :force => true do |t|
    t.integer  "user_id"
    t.integer  "report_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "user_profiles", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "allow_shared_reports", :default => true
    t.string   "avatar_url"
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
    t.string   "time_zone",              :default => "Eastern Time (US & Canada)"
    t.string   "email",                  :default => "",                           :null => false
    t.string   "encrypted_password",     :default => "",                           :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,                            :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0,                            :null => false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "fname"
    t.string   "lname"
    t.integer  "roles_mask"
    t.boolean  "allow_login",            :default => true
    t.datetime "password_changed_at"
    t.string   "provider"
    t.integer  "uid"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["password_changed_at"], :name => "index_users_on_password_changed_at"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
