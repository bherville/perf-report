class AddGraphCommandColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :graph_command, :string
  end
end
