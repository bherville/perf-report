class CreateServerGroupProfiles < ActiveRecord::Migration
  def change
    create_table :server_group_profiles do |t|
      t.integer :server_id
      t.integer :server_group_id

      t.timestamps
    end
  end
end
