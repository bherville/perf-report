class AddSendDayColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :send_day, :string
  end
end
