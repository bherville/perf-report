class DropSeverRegexesTable < ActiveRecord::Migration
  def up
    drop_table :server_regex_user_profiles
    drop_table :server_regexes
  end

  def down
    create_table :server_regex_user_profiles do |t|
      t.integer :user_profile_id
      t.integer :server_regex_id

      t.timestamps
    end

    create_table :server_regexes do |t|
      t.string :name
      t.text :regex

      t.timestamps
    end
  end
end
