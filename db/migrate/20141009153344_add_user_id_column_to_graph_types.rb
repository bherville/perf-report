class AddUserIdColumnToGraphTypes < ActiveRecord::Migration
  def change
    add_column :graph_types, :user_id, :integer
  end
end
