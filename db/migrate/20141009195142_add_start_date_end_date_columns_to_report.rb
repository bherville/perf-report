class AddStartDateEndDateColumnsToReport < ActiveRecord::Migration
  def change
    add_column :reports, :start_date, :timestamp
    add_column :reports, :end_date, :timestamp
  end
end
