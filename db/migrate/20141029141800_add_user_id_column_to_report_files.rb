class AddUserIdColumnToReportFiles < ActiveRecord::Migration
  def change
    add_column :report_files, :user_id, :integer
  end
end
