class AddLastSentColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :last_time_sent, :timestamp
  end
end
