class CreateReportApplications < ActiveRecord::Migration
  def change
    create_table :report_applications do |t|
      t.integer :report_id
      t.integer :application_id
      t.integer :user_id

      t.timestamps
    end
  end
end
