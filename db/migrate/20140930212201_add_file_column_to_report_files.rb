class AddFileColumnToReportFiles < ActiveRecord::Migration
  def self.up
    add_attachment :report_files, :file
  end

  def self.down
    remove_attachment :report_files, :file
  end
end
