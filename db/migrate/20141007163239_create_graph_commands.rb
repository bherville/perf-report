class CreateGraphCommands < ActiveRecord::Migration
  def change
    create_table :graph_commands do |t|
      t.string :name
      t.string :command
      t.integer :user_id

      t.timestamps
    end
  end
end
