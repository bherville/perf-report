class CreateApplicationFamilyApplications < ActiveRecord::Migration
  def change
    create_table :application_family_applications do |t|
      t.boolean :ignore_active
      t.boolean :active
      t.integer :application_id
      t.integer :application_family_id
      t.integer :user_id

      t.timestamps
    end
  end
end
