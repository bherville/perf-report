class RemoveGraphTypesColumnFromReports < ActiveRecord::Migration
  def up
    remove_column :reports, :graph_types
  end

  def down
    add_column :reports, :graph_types, :text
  end
end
