class CreateGraphTypeReports < ActiveRecord::Migration
  def change
    create_table :graph_type_reports do |t|
      t.integer :graph_type_id
      t.integer :report_id

      t.timestamps
    end
  end
end
