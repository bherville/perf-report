class CreateApplicationFamilies < ActiveRecord::Migration
  def change
    create_table :application_families do |t|
      t.string :name
      t.boolean :ignore_active
      t.boolean :active
      t.integer :user_id

      t.timestamps
    end
  end
end
