class CreateReportFiles < ActiveRecord::Migration
  def change
    create_table :report_files do |t|
      t.integer :report_id

      t.timestamps
    end
  end
end
