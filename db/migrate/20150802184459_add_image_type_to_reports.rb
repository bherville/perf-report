class AddImageTypeToReports < ActiveRecord::Migration
  def change
    add_column :reports, :image_type, :string
  end
end
