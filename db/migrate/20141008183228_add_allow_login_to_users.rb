class AddAllowLoginToUsers < ActiveRecord::Migration
  def change
    add_column :users, :allow_login, :boolean, default: true
  end
end
