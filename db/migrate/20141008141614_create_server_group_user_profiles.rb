class CreateServerGroupUserProfiles < ActiveRecord::Migration
  def change
    create_table :server_group_user_profiles do |t|
      t.integer :server_group_id
      t.integer :user_profile_id

      t.timestamps
    end
  end
end
