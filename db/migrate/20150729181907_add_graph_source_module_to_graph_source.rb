class AddGraphSourceModuleToGraphSource < ActiveRecord::Migration
  def change
    add_column :graph_sources, :graph_source_module, :string
    add_column :graph_sources, :graph_source_module_parameters, :text
  end
end
