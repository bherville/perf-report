class RemoveInfluxdbColumnsFromGraphTypes < ActiveRecord::Migration
  def up
    remove_column :graph_types, :influxdb_command
    remove_column :graph_types, :influxdb_graph_name
  end

  def down
    add_column :graph_types, :influxdb_command, :text
    add_column :graph_types, :influxdb_graph_name, :text
  end
end
