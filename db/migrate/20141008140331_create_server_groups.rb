class CreateServerGroups < ActiveRecord::Migration
  def change
    create_table :server_groups do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
