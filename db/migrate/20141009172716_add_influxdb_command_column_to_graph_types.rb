class AddInfluxdbCommandColumnToGraphTypes < ActiveRecord::Migration
  def change
    add_column :graph_types, :influxdb_command, :text
  end
end
