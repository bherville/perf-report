class CreateReportFileEmails < ActiveRecord::Migration
  def change
    create_table :report_file_emails do |t|
      t.integer :report_file_id
      t.text :email_addresses
      t.boolean :mailing_list_send, :default => false

      t.timestamps
    end
  end
end
