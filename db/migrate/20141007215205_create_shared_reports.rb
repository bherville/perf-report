class CreateSharedReports < ActiveRecord::Migration
  def change
    create_table :shared_reports do |t|
      t.integer :user_id
      t.integer :report_id

      t.timestamps
    end
  end
end
