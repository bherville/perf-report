class AddScheduleReportRunColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :schedule_report_run, :boolean, :default => true
  end
end
