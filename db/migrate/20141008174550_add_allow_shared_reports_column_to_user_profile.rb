class AddAllowSharedReportsColumnToUserProfile < ActiveRecord::Migration
  def change
    add_column :user_profiles, :allow_shared_reports, :boolean, :default => true

  end
end
