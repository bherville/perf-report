class AddAvatarUrlToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :avatar_url, :string
  end
end
