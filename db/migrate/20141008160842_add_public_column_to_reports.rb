class AddPublicColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :public, :boolean, :default => false
  end
end
