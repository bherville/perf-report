class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :name
      t.string :app_type
      t.boolean :major_application
      t.boolean :ignore_active
      t.boolean :active
      t.integer :user_id

      t.timestamps
    end
  end
end
