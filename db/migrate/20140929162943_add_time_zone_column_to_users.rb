class AddTimeZoneColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :time_zone, :string, :default => Time.zone.name
  end
end
