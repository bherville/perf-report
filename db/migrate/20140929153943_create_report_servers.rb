class CreateReportServers < ActiveRecord::Migration
  def change
    create_table :report_servers do |t|
      t.integer :server_id
      t.integer :report_id

      t.timestamps
    end
  end
end
