class RemoveHardCodedFromGraphSources < ActiveRecord::Migration
  def up
    remove_column :graph_sources, :username
    remove_column :graph_sources, :password
    remove_column :graph_sources, :database
    remove_column :graph_sources, :host
    remove_column :graph_sources, :port
    remove_column :graph_sources, :connection_retries
    remove_column :graph_sources, :test_query
  end

  def down
    add_column :graph_sources, :username, :string
    add_column :graph_sources, :password, :string
    add_column :graph_sources, :database, :string
    add_column :graph_sources, :host, :string
    add_column :graph_sources, :port, :integer
    add_column :graph_sources, :connection_retries, :integer
    add_column :graph_sources, :test_query, :text
  end
end
