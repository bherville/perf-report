class AddGraphTypeIdColumnToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :graph_type_id, :integer
  end
end
