class AddDisplayOrderToGraphTypes < ActiveRecord::Migration
  def change
    add_column :graph_types, :display_order, :integer
  end
end
