class AddIndexToGraphs < ActiveRecord::Migration
  def change
    add_index 'graphs', ['name', 'server_id'], :name => 'server_graphs'
  end
end
