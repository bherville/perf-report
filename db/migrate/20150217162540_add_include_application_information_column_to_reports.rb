class AddIncludeApplicationInformationColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :include_application_information, :boolean, :default => false
  end
end
