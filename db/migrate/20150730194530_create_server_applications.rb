class CreateServerApplications < ActiveRecord::Migration
  def change
    create_table :server_applications do |t|
      t.text :description
      t.boolean :ignore_active
      t.boolean :active
      t.integer :application_id
      t.integer :server_id
      t.integer :user_id

      t.timestamps
    end
  end
end
