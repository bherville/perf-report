class CreateGraphs < ActiveRecord::Migration
  def change
    create_table :graphs do |t|
      t.string :name
      t.string :file_path
      t.integer :server_id

      t.timestamps
    end
  end
end
