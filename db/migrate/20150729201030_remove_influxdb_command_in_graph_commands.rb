class RemoveInfluxdbCommandInGraphCommands < ActiveRecord::Migration
  def up
    remove_column :graph_commands, :influxdb_command
    remove_column :graph_commands, :influxdb_graph_data_interval_command
  end

  def down
    add_column :graph_commands, :influxdb_command, :text
    add_column :graph_commands, :influxdb_graph_data_interval_command, :text
  end
end
