class CreateGraphSources < ActiveRecord::Migration
  def change
    create_table :graph_sources do |t|
      t.string :name
      t.string :username
      t.string :password
      t.string :database
      t.string :host
      t.integer :port
      t.integer :connection_retries, :default => 4
      t.text :test_query, :default => 'select * from /.*/ limit 1'
      t.boolean :default, :null => false, :default => false
      t.integer :user_id

      t.timestamps
    end
  end
end
