class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.timestamp :send_time
      t.text :mailing_list
      t.integer :user_id

      t.timestamps
    end
  end
end
