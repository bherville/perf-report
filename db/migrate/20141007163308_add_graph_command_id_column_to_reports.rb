class AddGraphCommandIdColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :graph_command_id, :integer
  end
end
