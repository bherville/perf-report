class CreateServerGroupReports < ActiveRecord::Migration
  def change
    create_table :server_group_reports do |t|
      t.integer :server_group_id
      t.integer :report_id

      t.timestamps
    end
  end
end
