class AddGraphSourceColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :graph_source_id, :integer
  end
end
