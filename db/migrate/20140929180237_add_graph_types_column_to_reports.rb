class AddGraphTypesColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :graph_types, :string
  end
end
