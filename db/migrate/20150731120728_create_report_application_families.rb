class CreateReportApplicationFamilies < ActiveRecord::Migration
  def change
    create_table :report_application_families do |t|
      t.integer :application_family_id
      t.integer :report_id
      t.integer :user_id

      t.timestamps
    end
  end
end
