class AddInfluxdbGraphDataIntervalCommandColumnToGraphCommands < ActiveRecord::Migration
  def change
    add_column :graph_commands, :influxdb_graph_data_interval_command, :text
  end
end
