class AddUserIdColumnToServerRegexes < ActiveRecord::Migration
  def change
    add_column :server_regexes, :user_id, :integer
  end
end
