class ChangeColumnNamesInGraphCommands < ActiveRecord::Migration
  def up
    rename_column :graph_commands, :command, :nagios_command
    add_column :graph_commands, :influxdb_command, :text
  end

  def down
    rename_column :graph_commands, :nagios_command, :command
    remove_column :graph_commands, :influxdb_command
  end
end
