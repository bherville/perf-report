class AddGraphSourceIdToGraphCommand < ActiveRecord::Migration
  def change
    add_column :graph_commands, :graph_source_id, :integer
  end
end
