class AddTimeZoneToReport < ActiveRecord::Migration
  def change
    add_column :reports, :time_zone, :string
  end
end
