class CreateReportGraphs < ActiveRecord::Migration
  def change
    create_table :report_graphs do |t|
      t.integer :report_id
      t.string :graph_id

      t.timestamps
    end
  end
end
