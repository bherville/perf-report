class AddInfluxdbGraphNameColumnToGraphType < ActiveRecord::Migration
  def change
    add_column :graph_types, :influxdb_graph_name, :string
  end
end
