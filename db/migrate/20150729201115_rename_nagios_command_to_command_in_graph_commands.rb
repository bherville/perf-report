class RenameNagiosCommandToCommandInGraphCommands < ActiveRecord::Migration
  def up
    rename_column :graph_commands, :nagios_command, :command
  end

  def down
    rename_column :graph_commands, :command, :nagios_command
  end
end
