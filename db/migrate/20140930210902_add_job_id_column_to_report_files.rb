class AddJobIdColumnToReportFiles < ActiveRecord::Migration
  def change
    add_column :report_files, :job_id, :string
  end
end
