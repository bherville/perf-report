class AddHasDatesColumnToGraphCommand < ActiveRecord::Migration
  def change
    add_column :graph_commands, :has_dates, :boolean
  end
end
