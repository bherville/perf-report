class RemoveAvailableServersColumnFrom < ActiveRecord::Migration
  def up
    remove_column :user_profiles, :available_servers
  end

  def down
    add_column :user_profiles, :available_servers, :text
  end
end
