class AddShowAllApplicationsToReports < ActiveRecord::Migration
  def change
    add_column :reports, :show_all_applications, :boolean, :default => true
  end
end
