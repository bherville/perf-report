DEMO_DATA = ENV["PERFREPORT_DEMO_DATA"].nil? ? true : ENV["PERFREPORT_DEMO_DATA"]

def demo_data?
  if DEMO_DATA == true || DEMO_DATA == 'true'
    true
  else
    false
  end
end

puts 'Creating System Models'
unless User.find_by_email(SYSTEM_USER_ACCOUNT_EMAIL)
  require 'securerandom'
  password = SecureRandom.hex(50)

  admin = User.new(
      :email => SYSTEM_USER_ACCOUNT_EMAIL,
      :password => password,
      :password_confirmation => password,
      :fname => 'Internal-System',
      :lname => 'User'
  )

  admin.roles << :admin
  admin.roles << :report_generator
  admin.build_user_profile
  admin.user_profile.allow_shared_reports = false
  admin.skip_confirmation!
  admin.save!
end

unless GraphCommand.find_by_name('Last 24 Hours')
  command = GraphCommand.new(
      :name             => 'Last 24 Hours',
      :nagios_command   => '-s now-1day -t hours',
      :influxdb         => 'time > now() - 24h',
      :account             => User.find_by_email(SYSTEM_USER_ACCOUNT_EMAIL)
  )

  command.save!


  puts '################################'
  puts "#{command.name} Graph Command Created"
  puts "Command: #{command.command}"
  puts '################################'
end

if !GraphSource.find_by_name('Nagios') && demo_data?
  GraphSource.create!(
      :name => 'Nagios'
  )
end

puts 'Creating Demo Data...' if demo_data?

if !User.find_by_email('admin@example.com') && demo_data?
  puts "Generating 'admin@example.com'"
  require 'securerandom'
  password = SecureRandom.hex(8)

  admin = User.new(
      :email => 'admin@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Admin',
      :lname => 'User'
  )

  admin.roles << :admin
  admin.roles << :report_generator
  admin.build_user_profile
  admin.skip_confirmation!
  admin.save!


  puts '################################'
  puts 'Administrator User Credentials'
  puts "Email Address: #{admin.email}"
  puts "Password: #{password}"
  puts '################################'
end

if !User.find_by_email('user@example.com') && demo_data?
  puts "Generating 'user@example.com'"
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test',
      :lname => 'User'
  )

  user.roles << :account
  user.roles << :report_generator
  user.build_user_profile
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end

if !User.find_by_email('user1@example.com') && demo_data?
  puts "Generating 'user1@example.com'"
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user1@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test1',
      :lname => 'User'
  )

  user.roles << :account
  user.build_user_profile
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end

if !User.find_by_email('user2@example.com') && demo_data?
  puts "Generating 'user2@example.com'"
  require 'securerandom'
  password = SecureRandom.hex(8)

  user = User.new(
      :email => 'user2@example.com',
      :password => password,
      :password_confirmation => password,
      :fname => 'Test2',
      :lname => 'User'
  )

  user.roles << :account
  user.build_user_profile
  user.skip_confirmation!
  user.save!


  puts '################################'
  puts "#{user.email} Credentials"
  puts "Email Address: #{user.email}"
  puts "Password: #{password}"
  puts '################################'
end

