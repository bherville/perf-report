module PerfReportExceptions
  module Report
    class InvalidGraphCommand < Exception
      def initialize(message)
        super
        @message = message
      end
    end

    class InvalidGraphSource < Exception
      def initialize(message)
        super
        @message = message
      end
    end
  end
end