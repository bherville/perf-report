namespace :server_groups do
  desc "Add all servers in the system to the server_group 'All Servers'. This can be overwritten by specifying the environment variable all_server_group"
  task :add_all_servers => :environment do
    all_server_group = ENV['all_server_group']
    all_server_group ||= 'All Servers'

    if ServerGroup.find_by_name(all_server_group).nil?
      p "The ServerGroup #{all_server_group} doesn't exist. Creating it..."
      ServerGroup.create!(
        :name  => all_server_group,
      )
    end

    all_servers = ServerGroup.find_by_name all_server_group
    new_servers = Array.new

    Server.all.each do |server|
      new_servers.push(server) if all_servers.servers.includes(server).empty?
    end



    all_servers.servers += new_servers

    if all_servers.save
      p "#{new_servers.count} servers added to the ServerGroup #{all_server_group}."
    else
      p "The servers could not be added to the ServerGroup #{all_server_group}."
    end
  end

end
