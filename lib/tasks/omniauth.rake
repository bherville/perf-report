namespace :omniauth do
  namespace :users do
    desc 'Import a JSON hash of users from AuthMan'
    task :import, [:json] => [:environment] do |t, args|
      auth_man_users = JSON.parse(File.read(args[:json]))

      auth_man_users.each do |k,attr|
        user = User.find_by_email(k)

        unless user
          puts "A user with the email address #{k} was not found. Skipping..."
          next
        end

        if attr['applications'].include?('PerfReport')
          puts "Setting uid (#{attr['uid']}) and provider (#{attr['provider']}) for the user with the email address #{k}."
          user.uid = attr['uid']
          user.provider = attr['provider']
          user.save!
        else
          puts "The user with the email address #{k} is not authorized to access this application. Skipping..."
          next
        end
      end
    end
  end
end
