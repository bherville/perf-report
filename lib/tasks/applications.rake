namespace :applications do
  desc 'Sync changes with the external APIs'
  task :sync_all => :environment do
    puts 'Syncing Application...'
    puts "#{Application.create_and_update.count.to_s} Application(s) Synchronized..."
    puts "#{Application.remove_stale.count.to_s} Application(s) Deleted..."
    puts ''
    puts 'Syncing Server Application...'
    puts "#{ServerApplication.create_and_update.count.to_s} Server Application(s) Synchronized..."
    puts "#{ServerApplication.remove_stale.count.to_s} Server Application(s) Deleted..."
    puts 'Syncing Application Family...'
    puts "#{ApplicationFamily.create_and_update.count.to_s} Application Family(ies) Synchronized..."
    puts "#{ApplicationFamily.remove_stale.count.to_s} Application Family(ies) Deleted..."
    puts ''
    puts 'Syncing Application Family Application...'
    puts "#{ApplicationFamilyApplication.create_and_update.count.to_s} Application Family Application(s) Synchronized..."
    puts "#{ApplicationFamilyApplication.remove_stale.count.to_s} Application Family Application(s) Deleted..."
    puts ''
  end
end