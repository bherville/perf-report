PerfReport::Application.routes.draw do
  resources :server_applications


  resources :applications


  require 'sidekiq/web'

  resources :graph_types
  resource :help, :controller => 'help' do
    get :about
    get :download_manual
  end


  devise_for :users, :controllers => { :users => 'users', :registrations => 'registrations', :confirmations => 'confirmations', :password_expired => 'password_expired', :omniauth_callbacks => 'users/omniauth_callbacks' }
  devise_scope :user do
    put '/confirm' => 'confirmations#confirm'
  end

  resource :account, :controller => :account, :only => [:show]  do
    resource :password_expired, :only => [:show, :update], :controller => :password_expired
  end

  resources :users, :only => [:show]

  authenticated :user do
    root :to => 'reports#index', :as => :authenticated_root
  end

  root :to => redirect('/users/sign_in')

  resources :reports do
    member do
      post :generate
    end
  end
  resources :graphs, :only => [:index, :show, :destroy]
  resources :servers, :only => [:index, :show, :destroy]
  resources :report_files do
    resources :report_file_emails, :only => [:new, :create, :send_email] do
      collection do
        get :send_email
      end
    end
  end
  resources :server_groups
  resources :graph_commands



  namespace :api, :defaults => {:format => :json}  do
    resources :reports do
      member do
        get :generate
        get :generate_status
        get :user_report_files
      end
    end
    resources :report_files

    resources :graph_sources do
      resources :graph_commands
    end
  end

  namespace :admin do
    match '/' => 'admin#index'
    resources :users,  :controllers => { :users => 'admin/users'}
    resources :graph_sources
  end

  authenticate :user, lambda { |u| u.has_role?(:admin) } do
    mount Sidekiq::Web => '/admin/sidekiq'
  end
end
