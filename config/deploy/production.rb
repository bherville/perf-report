set :rails_environment, 'production'
set :stage, 'production'

server '192.168.1.67', account: 'deploy', roles: %w{web app production_app primary_app}