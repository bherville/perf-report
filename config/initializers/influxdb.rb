def connect_to_influxdb
  init_graph_source
  influx_source = GraphSource.find_by_name 'InfluxDB'

  begin
    @influxDB.get_database_list
    @influxDB.query influx_source.test_query
    @influxDB
  rescue
    influxdb = InfluxDB::Client.new influx_source.database,
                                    :host    => influx_source.host,
                                    :username => influx_source.username,
                                    :password => influx_source.password,
                                    :retry    => influx_source.connection_retries

    @influxDB = influxdb
  end
end

def init_graph_source
  if GraphSource.table_exists? && GraphSource.find_by_name('InfluxDB')
    require 'influxdb'
  end
end

init_graph_source