GRAPH_SOURCE_MODULES_DIR = Rails.root.join('lib', 'graph_source_modules')
GRAPH_SOURCE_MODULES = Array.new

Dir[File.join(GRAPH_SOURCE_MODULES_DIR, '*', 'module.yml')].each do |file|
  module_info = YAML.load_file(file)
  GRAPH_SOURCE_MODULES << {
      :name         => module_info['graph_source_name'],
      :display_name => module_info['graph_source_display_name']
  }
end