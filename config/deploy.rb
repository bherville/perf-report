# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'perfreport'				                        # The application name
set :repo_url, 'git@bitbucket.org:bherring/perf-report.git'		# The repository to pull from
set :branch, ENV["TAG"] || 'master'				                    # The branch in the repository to deploy from
set :deploy_to, '/var/www/perfreport'				                  # The directory to deploy to
set :log_level, :debug						                            # The logging level for capistrano
set :keep_releases, 5						                              # The number of release to keep on the destination servers
set :gem_set, 'ruby-2.1.3@perfreport'	                        # The RVM gemset to use for this application
set :pty, true                                                # Fix for sudo error, sudo: no tty present and no askpass program






######################
# DO NOT EDIT BELLOW #
######################
# Shared
set :linked_files, %W{config/database.yml config/app_config.yml config/initializers/app_config.rb config/initializers/sidekiq.rb config/initializers/devise.rb config/environments/#{fetch(:stage)}.rb}
set :linked_dirs, %w{log public/system}

# RVM
set :rvm_type, :system
set :rvm_ruby_version, fetch(:gem_set)

# Rails
set :rails_env, fetch(:rails_environment)
set :migration_role, :primary_app
set :assets_roles, [:production_app]

# Bundler
set :bundle_roles, :app
set :bundle_jobs, 4
set :bundle_flags, '--quiet'

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app) do
      execute :touch, current_path.join('tmp/restart.txt')
      execute 'sudo /sbin/service perfreport-jobs restart'
      execute 'sudo /sbin/service perfreport-jobs-clockwork restart'
    end
  end

  desc 'Create Directories'
  task :create_directories do
    on roles(:app) do
      # Create the tmp directory
      execute "[ -d #{release_path.join('tmp')} ] || mkdir -p #{release_path.join('tmp')}"
      # Create the tmp/pids directory
      execute "[ -d #{release_path.join('tmp', 'pids')} ] || mkdir -p #{release_path.join('tmp', 'pids')}"
    end
  end

  desc 'Fix File Permissions'
  task :fix_file_permissions do
    on roles(:web) do
      execute "chmod -R 770 #{shared_path.join('public','system')}"
      execute "chmod 770 #{release_path.join('tmp')}"
    end
  end

  desc 'Upload Configurations'
  task :upload_configurations do
    on roles(:app) do
      # Create the config directory if it doesn't exist
      execute "[ -d #{shared_path.join('config')} ] || mkdir -p #{shared_path.join('config')}"
      execute "[ -d #{shared_path.join('config', 'initializers')} ] || mkdir -p #{shared_path.join('config', 'initializers')}"
      execute "[ -d #{shared_path.join('config', 'environments')} ] || mkdir -p #{shared_path.join('config', 'environments')}"

      # Upload the configuration files
      set :stage_source, "config/deploy/config/#{fetch(:stage)}"
      set :all_source, 'config/deploy/config/all'
      upload! "#{fetch(:stage_source)}/app_config.yml", shared_path.join('config/app_config.yml')
      upload! "#{fetch(:stage_source)}/database.yml", shared_path.join('config/database.yml')
      upload! "#{fetch(:all_source)}/initializers/app_config.rb", shared_path.join('config', 'initializers', 'app_config.rb')
      upload! "#{fetch(:stage_source)}/initializers/sidekiq.rb", shared_path.join('config/initializers/sidekiq.rb')
      upload! "#{fetch(:stage_source)}/initializers/devise.rb", shared_path.join('config/initializers/devise.rb')
      upload! "#{fetch(:stage_source)}/environments/#{fetch(:stage)}.rb", shared_path.join('config','environments',"#{fetch(:stage)}.rb")
    end
  end

  desc 'Setup RVM'
  task :setup_rvm do
    on roles(:app) do
      # Create the RVM gemset if it doesn't already exist
      execute '/usr/local/rvm/bin/rvm', "#{fetch(:gem_set)}", '--create', :do, :true

      # Add authman_base_url
      execute "grep -q -F \"export AUTHMAN_BASE_URL='#{fetch(:authman_base_url)}'\" /usr/local/rvm/gems/#{fetch(:gem_set)}/environment || echo \"export AUTHMAN_BASE_URL='#{fetch(:authman_base_url)}'\" >> /usr/local/rvm/gems/#{fetch(:gem_set)}/environment"

      if fetch(:GRAVATAR_DOMAIN)
      execute "grep -q -F \"export GRAVATAR_DOMAIN='#{fetch(:GRAVATAR_DOMAIN)}'\" /usr/local/rvm/gems/#{fetch(:gem_set)}/environment || echo \"export GRAVATAR_DOMAIN='#{fetch(:GRAVATAR_DOMAIN)}'\" >> /usr/local/rvm/gems/#{fetch(:gem_set)}/environment"
      end
    end
  end

  desc 'Seed Database'
  task :seed_db do
    on roles(:app) do
      #execute "cd #{release_path} && /usr/local/rvm/bin/rvm", "#{fetch(:gem_set)}", :do, 'rake db:seed'
      execute "cd #{release_path} && RAILS_ENV=#{fetch(:stage)} PERFREPORT_DEMO_DATA=false /usr/local/rvm/bin/rvm #{fetch(:gem_set)} do bundle exec rake db:seed"
    end
  end


  # Canstap hooks
  before 'rvm:hook', 'deploy:setup_rvm'
  before :starting, :upload_configurations
  after :updated, 'bundler:install'
  after :updated, :create_directories
  after :updated, :seed_db
  after :create_directories, :fix_file_permissions
  after :publishing, :restart
end
