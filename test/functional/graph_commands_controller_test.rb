require 'test_helper'

class GraphCommandsControllerTest < ActionController::TestCase
  setup do
    @graph_command = graph_commands(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:graph_commands)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create graph_command" do
    assert_difference('GraphCommand.count') do
      post :create, graph_command: { command: @graph_command.command, name: @graph_command.name }
    end

    assert_redirected_to graph_command_path(assigns(:graph_command))
  end

  test "should show graph_command" do
    get :show, id: @graph_command
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @graph_command
    assert_response :success
  end

  test "should update graph_command" do
    put :update, id: @graph_command, graph_command: { command: @graph_command.command, name: @graph_command.name }
    assert_redirected_to graph_command_path(assigns(:graph_command))
  end

  test "should destroy graph_command" do
    assert_difference('GraphCommand.count', -1) do
      delete :destroy, id: @graph_command
    end

    assert_redirected_to graph_commands_path
  end
end
