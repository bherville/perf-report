require 'test_helper'

class GraphSourcesControllerTest < ActionController::TestCase
  setup do
    @graph_source = admin_graph_sources_path(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:graph_source_modules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create graph_source" do
    assert_difference('GraphSource.count') do
      post :create, graph_source: { connection_retries: @graph_source.connection_retries, database: @graph_source.database, default: @graph_source.default, name: @graph_source.name, password: @graph_source.password, test_query: @graph_source.test_query, username: @graph_source.username }
    end

    assert_redirected_to admin_graph_sources_path(assigns(:graph_source))
  end

  test "should show graph_source" do
    get :show, id: @graph_source
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @graph_source
    assert_response :success
  end

  test "should update graph_source" do
    put :update, id: @graph_source, graph_source: { connection_retries: @graph_source.connection_retries, database: @graph_source.database, default: @graph_source.default, name: @graph_source.name, password: @graph_source.password, test_query: @graph_source.test_query, username: @graph_source.username }
    assert_redirected_to admin_graph_sources_path(assigns(:graph_source))
  end

  test "should destroy graph_source" do
    assert_difference('GraphSource.count', -1) do
      delete :destroy, id: @graph_source
    end

    assert_redirected_to admin_graph_sources_path
  end
end
