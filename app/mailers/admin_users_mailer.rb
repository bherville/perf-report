class AdminUsersMailer < ActionMailer::Base
  default from: APP_CONFIG['smtp']['from_address']

  def roles_changed_email(user)
    @user = user

    mail(:to =>@user.email, :subject => 'Performance Report - User Roles Updated')
  end

  def user_destroyed_email(user)
    @user = user

    mail(:to =>@user.email, :subject => 'Performance Report - User Account Deleted')
  end
end
