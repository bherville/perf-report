class ReportMailer < ActionMailer::Base
  default from: APP_CONFIG['smtp']['from_address']

  def pdf_email(report_file_email)
    @report_file_email = report_file_email

    recipients = @report_file_email.email_addresses.nil? || report_file_email.email_addresses.count == 0 ? Array.new : @report_file_email.email_addresses

    if (!report_file_email.report_file.report.nil? && !report_file_email.report_file.report.mailing_list.nil?) && @report_file_email.mailing_list_send
      recipients << @report_file_email.report_file.report.mailing_list
    end

    attachment = @report_file_email.report_file.compress_report

    if File.size(attachment[:path]) > APP_CONFIG['smtp']['max_attachment_size_bytes'].to_int
      @file_location_message = 'the report was too large to be sent via email. Please use the link to download the report.'
      @file_subject = "The report #{@report_file_email.report_file.report.name} has been generated."
    else
      @file_location_message = 'the same report is attached to this email'
      @file_subject = "Attached is the request performance report #{@report_file_email.report_file.report.name}."
      attachments[attachment[:file_name]] = File.read(attachment[:path])
    end

    mail(:to => recipients.join(','), :subject => "Performance Report - #{@report_file_email.report_file.report.name}")
  end
end
