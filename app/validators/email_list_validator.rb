class EmailListValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    rec = record
    att = attribute
    val = value

    list = email_string_to_array(val)

    if options.key?(:presence) && blank?(list)
      record.errors.add(attribute, :blank)
    end

    if options.key?(:valid) && (!blank?(list) && !valid_emails?(list))
      record.errors.add(attribute, :invalid_emails)
    end
  end

  def blank?(value)
    value.empty? || value.nil?
  end

  private
  def email_string_to_array(email_list)
    if email_list.is_a? String
      email_list.split("\r\n")
    else
      email_list
    end
  end

  def valid_emails?(list)
    invalid_email = false

    list.each do  |email|
      email_valid = (/#{Devise::email_regexp}/ =~ email)
      invalid_email = true if (email_valid != 0 || email_valid.nil?)
    end

    !invalid_email
  end
end