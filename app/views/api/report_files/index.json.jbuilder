json.report_files @report_files do |report_file|
  json.partial! 'api/report_files/report_file', report_file: report_file
end
