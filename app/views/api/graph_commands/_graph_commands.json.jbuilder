json.array!(graph_commands) do |graph_command|
  json.partial! 'api/graph_commands/graph_command', graph_command: graph_command
  json.url api_graph_source_graph_command_path(graph_command.graph_source, graph_command, format: :json)
end