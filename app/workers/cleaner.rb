class Cleaner
  include Sidekiq::Worker

  def perform(name = nil, count = nil)
    graphs = Graph.all

    graphs.each do |graph|
      unless File.exists?(graph.file_path)
        logger.info ("Destroying Graph: #{graph.name} - #{graph.server.name}. File no longer exists...")
        graph.destroy
      end
    end
  end

  # Create a redis client
  def redis
    @redis ||= Redis.new
  end
end