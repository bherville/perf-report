class Scan
  include Sidekiq::Worker

  def perform(name = nil, count = nil)
    graph_script_path = APP_CONFIG['graph_script_path']

    Dir.foreach(graph_script_path) do |item|
      next if item == '.' or item == '..'

      graph_attr = Graph.parse_file_name(item)

      if graph_attr
        server_name = graph_attr[:server_name].upcase
        server = Server.find_by_name(server_name)
        unless server
          server = Server.create :name => server_name
        end

        graph_type = GraphType.find_by_name(graph_attr[:graph_name])

        unless graph_type
          graph_type = GraphType.create(
            :name => graph_attr[:graph_name],
            :account => User.find_by_email(SYSTEM_USER_ACCOUNT_EMAIL)
          )
        end

        unless Graph.graph?(graph_attr[:graph_name], server.name)
          graph = Graph.create(
              :name       => graph_attr[:graph_name],
              :server     => server,
              :file_path  => File.join(graph_script_path, item),
              :graph_type => graph_type
          )

          logger.info ("Adding Graph: #{graph.name} - #{graph.server.name}")
        end
      end
    end
  end

  # Create a redis client
  def redis
    @redis ||= Redis.new
  end
end