class ReportGeneratorMail
  include Sidekiq::Worker
  sidekiq_options

  def perform(report_id, report_name, skip_mailing = false)
    logger.info "Processing report #{report_id}"
    report = Report.find(report_id)

    report_file = report.generate report_name, nil

    unless skip_mailing
      logger.info 'Sending report'

      report_file_email = report_file.report_file_emails.create!(
          :mailing_list_send  => true,
          :email_addresses  => []
      )

      ReportMailer.delay.pdf_email(report_file_email)
    end
  end

  # Create a redis client
  def redis
    @redis ||= Redis.new
  end
end