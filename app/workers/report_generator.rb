class ReportGenerator
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options

  def perform(report_id, report_name, user_id, email_address = nil)
    report = Report.find(report_id)
    user = User.find(user_id)

    p "Generating report for user: #{user.name} - #{user.id}"

    report_file = report.generate report_name, user, jid

    if email_address
      #TODO: Send report via email
      p "Sending report to user:#{email_address}"

      report_file_email = report_file.report_file_emails.create!(
          :mailing_list_send  => false,
          :email_addresses  => [email_address]
      )

      ReportMailer.delay.pdf_email(report_file_email)
    end
  end

  # Create a redis client
  def redis
    @redis ||= Redis.new
  end
end