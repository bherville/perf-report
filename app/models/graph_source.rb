class GraphSource < ActiveRecord::Base
  audited

  attr_accessible :name, :graph_source_module, :default, :graph_source_module_parameters

  validates :name, :presence => true,
                   :uniqueness => true
  validates :graph_source_module, :presence => true

  has_many :reports
  has_many :graph_commands, :dependent => :destroy

  belongs_to :user

  before_save :only_default
  before_save :parse_graph_source_module_parameters

  serialize :graph_source_module_parameters, Hash

  private
  def only_default
    if self.default?
      GraphSource.where('id <> ?', self.id).update_all(:default => false)

      self.default = true
    end
  end

  def parse_graph_source_module_parameters
    if self.graph_source_module_parameters.is_a? String
      parameters = Hash.new

      self.graph_source_module_parameters.split("\r\n").each do |parameter|
        key_value = parameter.split(':')

        parameters[key_value[0]] = key_value[1]
      end

      self.graph_source_module_parameters = parameters
    end
  end
end
