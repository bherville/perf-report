class ReportGraph < ActiveRecord::Base
  audited

  attr_accessible :graph_id, :report_id

  validates :report_id, presence: true
  validates :graph_id, presence: true
  validates_uniqueness_of :report_id, :scope => :graph_id

  belongs_to :graph
  belongs_to :report
end
