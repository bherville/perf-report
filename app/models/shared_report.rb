class SharedReport < ActiveRecord::Base
  audited

  attr_accessible :report_id, :user_id

  validates :report_id, :presence => true
  validates :user_id, :presence => true

  belongs_to :user
  belongs_to :report
end
