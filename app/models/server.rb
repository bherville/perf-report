class Server < ActiveRecord::Base
  audited

  attr_accessible :name

  validates :name, presence: true,
            uniqueness: true

  has_many :graphs
  has_many :report_servers
  has_many :reports, :through => :report_servers
  has_many :server_group_profiles
  has_many :server_groups, through: :server_group_profiles
  has_many :server_applications, :dependent => :destroy

  has_many :applications, :through => :server_applications

  def sorted_graphs
    default = 10000

    self.graphs.sort_by { |g| g.find_graph_type_from_name.display_order || default+1 } if self.graphs
  end
end
