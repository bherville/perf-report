class Graph < ActiveRecord::Base
  audited

  attr_accessible :file_path, :name, :server, :graph_type

  validates :name, presence: true
  validates :server_id, presence: true
  validates :file_path, presence: true

  validates_uniqueness_of :name, :scope => :server_id

  belongs_to :server
  belongs_to :graph_type

  def find_graph_type_from_name
    GraphType.find_by_name(self.name)
  end

  def self.graph? (name, server)
    server = Server.find_by_name server

    if server.nil?
      false
    else
      graph = Graph.find_by_name_and_server_id(name, server.id)


      if graph.nil?
        logger.debug("Graph Not Found: #{name} - #{server}")
        false
      else
        true
      end
    end
  end

  def self.parse_file_name (file_name)
    attr = file_name.split('_create_')

    { :server_name => attr[0], :graph_name => attr[1].gsub('.sh', '') } if attr.count == 2
  end
end
