class UserProfile < ActiveRecord::Base
  audited

  include Gravtastic
  gravtastic :email,
             :secure  => (AppConfig.get(%w(user_profiles secure), APP_CONFIG) ? AppConfig.get(%w(user_profiles secure), APP_CONFIG) : false),
             :rating  => 'PG'

  attr_accessible :user_id, :server_groups, :server_group_ids, :server_group_user_profiles, :allow_shared_reports,
                  :avatar_url

  belongs_to :user
  has_many :server_group_user_profiles
  has_many :server_groups, :through => :server_group_user_profiles

  def all_authorized_servers
    self.server_groups.map {|x| x.servers}.flatten
  end

  private
  def email
    self.user.email
  end
end
