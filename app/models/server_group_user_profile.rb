class ServerGroupUserProfile < ActiveRecord::Base
  audited

  attr_accessible :server_group_id, :user_profile_id

  belongs_to :server_group
  belongs_to :user_profile
end
