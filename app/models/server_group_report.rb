class ServerGroupReport < ActiveRecord::Base
  audited

  attr_accessible :report_id, :server_group_id

  belongs_to :report
  belongs_to :server_group
end
