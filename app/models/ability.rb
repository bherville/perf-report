class Ability
  include CanCan::Ability

  def initialize(user)
    models = [Report, ReportFile, Server]

    if user.nil?
      user = User.new
      current_user = user
    end

    if user.has_role?(:normal_user)
        can :read, [Report]
        can [:create, :send_email], ReportFileEmail
    end

    if user.has_role? :admin
      can :manage, [User, Admin, GraphSource]
    end

    if user.has_role? :report_generator
      can :manage, [Report]
      can :read, models
      can [:create, :send_email], ReportFileEmail
    end

    if user.has_role? :report_admin
      can :manage, [Report, Server, ServerGroup, Graph, GraphCommand, GraphType]
      can :read, models
      can [:create, :send_email], ReportFileEmail
    end
  end
end