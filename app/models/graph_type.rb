class GraphType < ActiveRecord::Base
  audited

  attr_accessible :name, :user, :display_order

  has_many :graph_type_reports
  has_many :graphs
  belongs_to :user

  def self.sorted
    default = 10000

    GraphType.all.sort_by { |g| g.display_order || default+1 }
  end

  def self.sort_by_display_order(graph_types)
    default = 10000

    graph_types.sort_by { |g| g.display_order || default+1 }
  end
end

