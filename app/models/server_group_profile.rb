class ServerGroupProfile < ActiveRecord::Base
  audited

  attr_accessible :server_group_id, :server_id

  belongs_to :server
  belongs_to :server_group
end
