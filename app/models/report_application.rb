class ReportApplication < ActiveRecord::Base
  audited

  attr_accessible :application_id, :report_id, :user_id

  belongs_to :application
  belongs_to :report
  belongs_to :user
end
