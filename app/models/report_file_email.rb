class ReportFileEmail < ActiveRecord::Base
  audited

  attr_accessible :email_addresses, :mailing_list_send, :report_file_id, :report_file

  validates :report_file_id, presence: true
  validate :email_addresses_indicated
  validates :email_addresses, email_list: { valid: true }

  belongs_to :report_file

  before_save :mailing_list_to_array
  before_update :mailing_list_to_array
  before_create :mailing_list_to_array

  serialize :email_addresses


  private
  def mailing_list_to_array
    if self.email_addresses.is_a? String
      self.email_addresses = self.email_addresses.split("\r\n")
    end
  end

  def email_addresses_indicated
    if self.mailing_list_send == false && (self.email_addresses.nil? || self.email_addresses.empty?)
      errors.add(:base, 'You must specify email addresses and/or check Send to Report Mailing List if available!')
    end
  end
end
