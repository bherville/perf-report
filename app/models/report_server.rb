class ReportServer < ActiveRecord::Base
  audited

  attr_accessible :report_id, :server_id

  validates :report_id, presence: true
  validates :server_id, presence: true
  validates_uniqueness_of :report_id, :scope => :server_id

  belongs_to :server
  belongs_to :report
end
