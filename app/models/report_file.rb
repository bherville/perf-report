require 'zip'

class ReportFile < ActiveRecord::Base
  audited

  attr_accessible :report_id, :file, :job_id, :report_file_emails, :user

  has_attached_file :file

  validates :report_id, :presence => true
  validates_attachment_content_type :file, :content_type => ['application/pdf']

  has_many :report_file_emails
  belongs_to :report
  belongs_to :user

  accepts_nested_attributes_for :report_file_emails

  def compress_report
    zip_file_name = "#{self.file_file_name}.zip"
    zip_tmp_dir = Rails.root.join('tmp', "#{Time.now.to_i.to_s}_report")
    Dir.mkdir zip_tmp_dir
    zip_file_path = zip_tmp_dir.join(zip_file_name)

    Zip::File.open(zip_file_path, Zip::File::CREATE) do |zip_file|
      zip_file.add(self.file_file_name, self.file.path)
    end

    { :path => zip_file_path, :file_name =>  zip_file_name }
  end

  def file_exists?
    if self.file.path.nil?
      false
    else
      File.exists?(self.file.path)
    end
  end
end
