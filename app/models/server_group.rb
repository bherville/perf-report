class ServerGroup < ActiveRecord::Base
  audited

  attr_accessible :name, :user_id, :server_ids

  validates :name, presence: true,
                   uniqueness: true

  belongs_to :user
  has_many :server_group_profiles
  has_many :servers, :through => :server_group_profiles
end
