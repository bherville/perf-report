class ReportApplicationFamily < ActiveRecord::Base
  audited

  attr_accessible :application_family_id, :report_id, :user_id

  validates :application_family_id, :presence => true
  validates :report_id, :presence => true

  belongs_to :application_family
  belongs_to :report
  belongs_to :user
end
