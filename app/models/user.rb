class User < ActiveRecord::Base
  audited

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :async, :registerable,
         :recoverable, :rememberable, :trackable,
         :confirmable, :lockable

  devise :omniauthable, :omniauth_providers => [:auth_man]

  include RoleModel

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :time_zone, :fname, :lname, :roles, :role_mask,
                  :user_profile, :user_profile_attributes, :encrypted_password, :password_salt

  validates :fname, :presence => true
  validates :lname, :presence => true

  validate :limited_servers

  # optionally set the integer attribute to store the roles in,
  # :roles_mask is the default
  roles_attribute :roles_mask

  # declare the valid roles -- do not change the order if you add more
  # roles later, always append them at the end!
  roles :admin, :report_generator, :report_admin, :limited_servers, :normal_user

  has_one :user_profile

  has_many :reports
  has_many :server_regexes
  has_many :graph_commands
  has_many :shared_reports
  has_many :server_groups
  has_many :graph_types
  has_many :graph_sources
  has_many :report_files
  has_many :applications
  has_many :server_applications
  has_many :report_applications
  has_many :application_family
  has_many :application_family_application
  has_many :report_application_family

  accepts_nested_attributes_for :user_profile

  before_save :add_default_roles
  after_save :mailer
  after_destroy :destroy_mailer
  after_destroy :orphan_reports
  after_create :setup_profile

  DEFAULT_ROLES = [
      :normal_user
  ]

  ROLE_DESCRIPTIONS = {
      :admin  => 'Can create/edit/delete users.',
      :report_generator  => 'Can create and edit/delete their own reports.',
      :report_admin  => 'Can create and edit/delete any reports.',
      :limited_servers  => 'Can only create reports for a subset of servers.',
      :normal_user  => 'Can generate preexisting reports.'
  }

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def full_name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : nil
  end

  def full_name_comma
    self.fname && self.lname ? "#{self.lname}, #{self.fname}" : nil
  end

  def name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : self.email
  end

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  private
  def valid_roles
    if self.roles_mask.nil? || self.roles_mask < 1
      errors.add(:base, 'At least one role must be selected')
    end
  end

  def add_default_roles
    DEFAULT_ROLES.each do |role|
      self.roles << role unless self.has_role?(role)
    end
  end

  def mailer
    if self.roles_mask_changed? && self.confirmed?
      AdminUsersMailer.delay.roles_changed_email(self)
    end
  end

  def destroy_mailer
    AdminUsersMailer.delay.user_destroyed_email(self)
  end

  def orphan_reports
    self.reports.each do |report|
      report.user = User.find_by_email(SYSTEM_USER_ACCOUNT_EMAIL)
      report.save
    end
  end

  def limited_servers
    if self.has_role?(:limited_servers) && (self.user_profile.server_groups.nil? || self.user_profile.server_groups.empty?)
      self.errors.add(:server_groups, 'invalid. If you select the limited servers role, you must specify at least one server group the user can have access to.')
    end
  end

  def setup_profile
    self.build_user_profile
    self.save
  end
end
