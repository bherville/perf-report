class GraphCommand < ActiveRecord::Base
  audited

  attr_accessible :command, :name, :user, :has_dates, :graph_source_id

  validates :name, :presence => true
  validates :graph_source_id, :presence => true

  validates_uniqueness_of :name, :scope => :graph_source_id

  validate :valid_command

  has_many :reports, :dependent => :nullify

  belongs_to :graph_source
  belongs_to :user

  def to_s
    "#{self.graph_source.name} - #{self.name}"
  end

  private
  def valid_command
    if self.command.nil? || self.command.empty?
      self.errors.add(:base, 'You must specify a command.')
    end
  end
end
