class Report < ActiveRecord::Base
  audited

  attr_accessible :mailing_list, :name, :send_time, :user_id, :send_day,
                  :servers, :server_ids, :graph_command, :last_time_sent,
                  :report_title, :schedule_report_run, :graph_command_id, :shared_reports,
                  :users, :user_ids, :server_groups, :server_group_ids, :public, :graph_types,
                  :graph_type_ids, :graph_source, :graph_source_id, :include_application_information,
                  :show_all_applications, :application_ids, :application_family_ids, :time_zone,
                  :image_type

  validates :name, presence: true, uniqueness: true
  validate :has_servers
  validates :graph_command, presence: true
  validate :has_graph_types
  validate :valid_day
  validate :valid_schedule
  validates :mailing_list, email_list: { valid: true }, :allow_blank => true
  validates :graph_source_id, :presence => true

  validate :valid_graph_command

  belongs_to :user
  belongs_to :graph_command
  belongs_to :graph_source

  has_many :report_files
  has_many :report_servers
  has_many :shared_reports
  has_many :server_group_reports
  has_many :graph_type_reports
  has_many :report_applications
  has_many :report_application_families

  accepts_nested_attributes_for :report_servers
  accepts_nested_attributes_for :shared_reports
  accepts_nested_attributes_for :report_applications
  accepts_nested_attributes_for :report_application_families

  has_many :server_groups, :through => :server_group_reports
  has_many :servers, :through => :report_servers
  has_many :users, :through => :shared_reports
  has_many :graph_types, :through => :graph_type_reports
  has_many :applications, :through => :report_applications
  has_many :application_families, :through => :report_application_families


  before_save :mailing_list_to_array
  before_save :clean_up_servers
  before_save :set_time_zone
  before_update :mailing_list_to_array
  before_create :mailing_list_to_array

  after_find :report_title_parser

  serialize :mailing_list
  serialize :graph_types

  DAYS = [
      :all,
      :sunday,
      :monday,
      :tuesday,
      :wednesday,
      :thursday,
      :friday,
      :saturday
  ]

  IMAGE_TYPES = {
      :png  => {
          :name       => 'PNG',
          :extension  => '.png',
      },
      :svg  => {
          :name       => 'SVG',
          :extension  => '.svg',
      },
      :default  => :png,
  }

  def reset_sent_date
    self.last_time_sent = nil

    self.save
  end

  def generate_graphs(tmp_graph_dir)
    raise PerfReportExceptions::Report::InvalidGraphCommand.new("Report #{self.id} has an invalid GraphCommand") unless self.graph_command

    require self.graph_source.graph_source_module

    servers_to_graph = self.servers_to_include

    graph_source_module_class = self.graph_source.graph_source_module.camelize.classify

    report_hash = HashWithIndifferentAccess.new(self.attributes.merge({
                                            :graph_command    => self.graph_command.command,
                                            :graph_types      => self.graph_types,
                                            :logger           => Rails.logger,
                                            :image_type       => get_image_type,
                                        }))
    report_hash['time_zone'] = ActiveSupport::TimeZone.find_tzinfo(self.time_zone).name

    graph_source_module = "#{graph_source_module_class}::GraphSource".constantize.new(report_hash)

    graph_source_module.generate_graphs(tmp_graph_dir, servers_to_graph)
  end


  def servers_to_include
    (server_group_servers + application_servers + application_families_servers + self.servers).uniq.sort! { |a,b| a.name.downcase <=> b.name.downcase }
  end

  def server_group_servers
    self.server_groups.map {|x| x.servers}.flatten
  end

  def application_servers
    self.applications.map {|a| a.servers}.flatten
  end

  def application_families_servers
    (self.application_families.map { |a| a.applications }.flatten).map { |a| a.servers }.flatten
  end

  def setup_pdf(debug = false)
    timestamp = Time.now

    tmp_graph_dir = Rails.root.join('tmp',"#{SecureRandom.hex(10)}-#{timestamp.to_i}_graphs")

    logger.info "Generating graphs to #{tmp_graph_dir}..."
    graphs = self.generate_graphs(tmp_graph_dir)

    logger.info 'Rendering PDF Report...'

    app_info = get_server_app_info if ((APP_CONFIG['application_info'] && APP_CONFIG['application_info']['embed_in_reports']) && self.include_application_information?)


    {
      :graphs         => graphs,
      :app_info       => app_info,
      :tmp_graph_dir  => tmp_graph_dir,
      :debug          => debug
    }
  end

  def generate(report_name, user, jid = nil)
    if jid.nil?
      report_file = self.report_files.create(
          :user => user
      )
    else
      report_file = self.report_files.create(
          :user => user,
          :job_id => jid
      )
    end


    report_file.save!

    pdf_hash = setup_pdf

    # create an instance of ActionView, so we can use the render method outside of a controller
    av = ActionView::Base.new
    av.view_paths = ActionController::Base.view_paths

    # need these in case your view constructs any links or references any helper methods.
    av.class_eval do
      include Rails.application.routes.url_helpers
      include ApplicationHelper
    end

    pdf_html = av.render :template => 'reports/generate.pdf.erb',
              :layout => nil,
              :locals => {
                  :report    => self,
                  :graphs    => pdf_hash[:graphs],
                  :app_info  => pdf_hash[:app_info],
              }

    # use wicked_pdf gem to create PDF from the doc HTML
    doc_pdf = WickedPdf.new.pdf_from_string(pdf_html, { :page_size => 'A4', :disable_smart_shrinking => true })

    # save PDF to disk
    report_path = File.join(pdf_hash[:tmp_graph_dir], report_name)
    logger.info "Saving PDF Report to #{report_path}..."
    File.open(report_path, 'wb') do |file|
      file << doc_pdf
    end

    file = File.open(report_path)
    report_file.file = file
    file.close

    report_file.save!

    logger.info 'Deleting temp graph directory...'
    FileUtils.rm_rf(pdf_hash[:tmp_graph_dir]) unless AppConfig.get(%w(reports debug), APP_CONFIG) || pdf_hash[:debug]

    report_file
  end

  # Query AppInfo and get all of the servers associated with this Reports information.
  def get_server_app_info
    url = "#{APP_CONFIG['application_info']['api_url']}/servers.json"
    json = RestClient.get url, {:accept => :json}

    servers_appinfo = JSON.parse(json)

    server_applications = Hash.new
    servers_appinfo.each do |server_appinfo|
      self.servers_to_include.each do |server|
        next unless server.name.upcase == server_appinfo['name'].upcase
        server_applications[server_appinfo['name'].upcase] = server_appinfo['server_applications']
      end
    end

    server_applications
  end

  def can_read?(user)
    self.public? || user.reports.include?(self) || self.users.include?(user) || user.has_role?(:report_admin)
  end

  def can_modify?(user)
   user.reports.include?(self) || user.has_role?(:report_admin)
  end

  def can_modify_or_read?(user)
    self.can_read?(user) || self.can_modify?(user)
  end

  def has_mailing_list?
    if self.mailing_list.nil? || self.mailing_list.empty?
      false
    else
      true
    end
  end

  def get_image_type
    img_type = self.image_type.to_sym if self.image_type
    img_type ||= Report::IMAGE_TYPES[AppConfig.get(%w(reports default_image_type), APP_CONFIG).to_sym] if AppConfig.get(%w(reports default_image_type), APP_CONFIG)
    img_type ||= Report::IMAGE_TYPES[Report::IMAGE_TYPES[:default]]

    img_type
  end

  private
  def valid_day
    if (self.send_day.nil? && DAYS.include?(self.send_day)) && self.schedule_report_run
      self.errors.add(:send_day, 'is invalid.')
    end
  end

  def mailing_list_to_array
    if self.mailing_list.is_a? String
      self.mailing_list = self.mailing_list.split("\r\n")
    end
  end

  def valid_emails?(list)
    if list
      invalid_email = false

      list.each do  |email|
        email_valid = (/#{Devise::email_regexp}/ =~ email)
        invalid_email = true if (email_valid != 0 || email_valid.nil?)
      end

      !invalid_email
    else
      true
    end
  end

  def report_title_parser
    if self.report_title.nil?
    else
      attributes = {
          '%name%'  => self.name,
          '%send_time%'  => self.send_time.to_s,
          '%send_day%'  => self.send_day.to_s,
          '%graph_types%'  => self.graph_types.split('\n').join(', '),
      }

      attributes['%graph_command%'] = self.graph_command.name unless self.graph_command.nil?

      attributes.each do |attr,value|
        self.report_title = self.report_title.gsub(attr, value)
      end
    end
  end

  def valid_day_time?
    (!self.send_day.nil? && !self.send_time.nil?)
  end

  def valid_schedule
    valid_day_time = valid_day_time?
    run_report =  self.schedule_report_run

    if !valid_day_time && run_report
      self.errors.add(:base, 'You must specify either a send day and send time or uncheck schedule report run.')
    end

    if run_report
      if self.mailing_list && !self.mailing_list.empty? &&
          valid_emails?(mailing_list_to_array)
      end
    end
  end

  def has_graph_types
    if self.graph_types.nil? || self.graph_types.empty?
      self.errors.add(:graph_types, 'missing. You must specify at least one graph type.')
    end
  end

  def has_servers
    if self.server_groups.empty? && self.servers.empty? && self.applications.empty? && self.application_families.empty?
      self.errors.add(:base, 'You must specify at least one server group or at least one server.')
    end
  end

  def clean_up_servers
    cleaned_servers = Array.new

    server_group_servers = self.server_groups.map {|x| x.servers}.flatten
    application_servers = self.applications.map {|a| a.servers}.flatten
    application_families = application_families_servers

    self.servers.each do |server|
      cleaned_servers << server unless server_group_servers.include?(server) || application_servers.include?(server) || application_families.include?(server)
    end

    self.servers = cleaned_servers
  end

  def valid_graph_command
    raise PerfReportExceptions::Report::InvalidGraphSource.new("Report #{self.id} has an invalid GraphSource") unless self.graph_source

    self.errors.add(:graph_command, "must be associated with the specified GraphSource '#{self.graph_source.name}'.") unless self.graph_source.graph_commands.include?(self.graph_command)
  end

  def set_time_zone
    self.time_zone ||= Time.zone.to_s
  end
end