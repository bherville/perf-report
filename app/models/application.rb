class Application < ActiveRecord::Base
  audited

  attr_accessible :active, :app_type, :ignore_active, :major_application, :name

  validates :app_type,  :presence => true
  validates :name,      :presence => true,
            :uniqueness => true

  APP_INFO_END_POINT = 'applications'

  belongs_to :user

  has_many :report_applications
  has_many :application_family_applications

  has_many :server_applications, :dependent => :destroy

  has_many :application_families, :through => :application_family_applications
  has_many :servers, :through => :server_applications

  def self.create_and_update
    created_updated = Array.new

    attr = {
        :name              => 'name',
        :app_type          => 'app_type',
        :major_application => 'major_application',
    }

    resources = AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, Application, attr, 'name'

    if resources
      resources.each do |resource_title, resource|
        current_resource = find_by_name(resource_title)

        if current_resource
          unless (current_resource.app_type == resource.app_type) &&
              (current_resource.major_application == resource.major_application)
            # Update the existing resource
            current_resource.app_type           = resource.app_type           unless current_resource.app_type == resource.app_type
            current_resource.major_application  = resource.major_application  unless current_resource.major_application == resource.major_application

            created_updated << current_resource
            current_resource.save
          end
        else
          created_updated << resource
          resource.save
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = (AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, Application, {:name => 'name'}, 'name').map { |n,r| r.name.downcase }

    resources = all.map { |r| r.name.downcase }

    to_delete = (resources - api_resources)

    to_delete.each do |r|
      find_by_name(r).delete
    end

    to_delete
  end
end
