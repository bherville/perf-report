class GraphTypeReport < ActiveRecord::Base
  audited

  attr_accessible :graph_type_id, :report_id

  belongs_to :graph_type
  belongs_to :report
end
