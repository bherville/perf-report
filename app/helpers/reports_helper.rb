module ReportsHelper
  def modify_allowed?(report)
    current_user.reports.include?(report) || current_user.has_role?(:report_admin)
  end

  def report_groups(report)
    group_str = Array.new

    report.applications.each do |application|
      group_str << "Application - #{application.name}"
    end

    report.application_families.each do |application_family|
      group_str << "Application Family - #{application_family.name}"
    end

    report.server_groups.each do |server_group|
      group_str << link_to("Server Group - #{server_group.name}", server_group)
    end

    group_str
  end

  def graph_image_types

  end
end
