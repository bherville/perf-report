module Admin::GraphSourcesHelper
  def graph_source_module_display_name(module_name)
    GRAPH_SOURCE_MODULES.select{ |m| m[:name] = module_name }.first[:display_name]
  end

  def default_graph_source
    default = (GraphSource.all.select { |s| s.default? }).flatten

    if default && default.count > 0
      default[0].id
    else
      nil
    end
  end

  def parse_graph_source_module_parameters(graph_source)
    parameters_string = ''

    if graph_source.graph_source_module_parameters.is_a? Array
      graph_source.graph_source_module_parameters.each do |key,value|
        parameters_string += "#{key}:#{value}"
      end
    end

    parameters_string
  end
end
