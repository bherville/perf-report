module ServersHelper
  def show_application_information?
    APP_CONFIG['application_info'].present? && APP_CONFIG['application_info']['enabled']
  end

  def application_information_api_url(server)
    "#{application_information_url}/api/servers/show?name=#{server.name}"
  end

  def application_information_url
    APP_CONFIG['application_info']['api_url']
  end
end
