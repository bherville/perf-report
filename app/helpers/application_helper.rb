module ApplicationHelper
  def current_users_home
    if !current_user.nil? && (current_user.has_role?(:admin) && current_user.roles.count == 1)
      admin_path
    else
      root_path
    end
  end

  def bool_to_word(boolean)
    if boolean
      'yes'
    else
      'no'
    end
  end

  def edit_html(path, class_type)
    html = ''
    if can?(:edit, class_type)
      html = "#{link_to t('general.edit'), path} |".html_safe
    end

    html
  end

  def user_public_link(user)
   link_to user.name, user_path(user),
            :title    => '',
            :class    => 'popover_link',
            :data     => {
                :content  => render('users/public_popover',
                                     :user    => user
                ).html_safe,
            }
  end

  def avatar_url(user, size = 80)
    if user.user_profile
      if user.user_profile.avatar_url.present?
        user.user_profile.avatar_url
      else
        user.user_profile.gravatar_url(:size => size)
      end
    end
  end

  def show_avatar
    APP_CONFIG["user_profiles"].present? && APP_CONFIG["user_profiles"]["show_avatars"]
  end

  def branding_title
    title = ''

    title = "#{APP_CONFIG['branding']['title']} - " if APP_CONFIG['branding'] && APP_CONFIG['branding']['title']

    "#{title}#{t('perfreport.perfreports')}"
  end
end