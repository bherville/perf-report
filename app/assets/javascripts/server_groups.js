$(document).ready(function() {
    initializeServerGroupsTable();
});

function initializeServerGroupsTable() {
    if ($('#server_groups_table').length) {
        $('#server_groups_table').dataTable(
            {
                "sScrollY": "400px",
                "bPaginate": true,
                "bDestroy": true,
                "iDisplayLength": 15,
                "aLengthMenu": [[15, 25, 50, 100, 200, -1], [15, 25, 50, 100, 200, "All"]]
            }).fnSort([[0, 'asc']]);
    }
}