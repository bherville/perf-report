$(document).ready(function(){
    initializeReportFilesTable();
});

function update_report_files_table(report_file) {
    var data = [
        '<a href="' + report_file.file_path + '" target="_blank">' + report_file.file_name  + '</a>' + '<span class="new_report_msg">NEW!</span>',
        report_file.file_generated_at,
        '<a href="/report_files/' + report_file.id + '/report_file_emails/new">Send via Email</a> <a href="/report_files/' + report_file.id + '">Details</a>'
    ];
    $("#report_files_table").dataTable().fnAddData(data);
}

function initializeReportFilesTable(iDisplayLength, aLengthMenu)
{
    iDisplayLength = iDisplayLength || 5;
    aLengthMenu = aLengthMenu || [[5, 10, 15, 20, 50, -1], [5, 10, 15, 20, 50, "All"]];

    if ($('#report_files_table').length) {
        $('#report_files_table').dataTable(
            {
                "sScrollY": "200px",
                "bPaginate": true,
                "bDestroy": true,
                "iDisplayLength": iDisplayLength,
                "aLengthMenu": aLengthMenu
            }).fnSort([[1, 'desc'], [0, 'desc']]);
    }
}