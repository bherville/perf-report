// User Form
var LIMITED_SERVERS_ROLE_BOX = 'role_limited_servers';
var AVAILABLE_SERVERS_DIV = "user_profile_available_servers";

$(document).ready(function() {
    setupUserForm();
});

function setupUserForm() {
    processUserForm();

    $('#' + LIMITED_SERVERS_ROLE_BOX).click(function () {
        processUserForm();
    });

}

function processUserForm() {
    var AVAILABLE_SERVERS_DIV = "user_profile_available_servers";
    var box = $('#' + LIMITED_SERVERS_ROLE_BOX).children('input')[0];

    console.log($(box).is(':checked'));

    if ($(box).is(':checked')) {
        $('#' + AVAILABLE_SERVERS_DIV).slideDown();
    }

    else {
        $('#' + AVAILABLE_SERVERS_DIV).slideUp();
    }

}