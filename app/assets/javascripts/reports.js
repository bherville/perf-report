var PENDING_REPORTS = 'pending_reports';
var WORKING_REPORTS = 'working_reports';
var STATUS_CHECK = "pending_reports_check";
var PENDING_REPORT_CHECK_ID = "pending_reports_check_id";
var LOGIN_PAGE = "/users/sign_in";

// Report Form
var SCHEDULE_REPORT_RUN_BOX = 'report_schedule_report_run';
var REPORT_SCHEDULE_DIV = "report_schedule";

$(document).ready(function() {
    // If we are not on the login page.
    if (window.location.pathname != LOGIN_PAGE) {
        // Hide the spinner on page load.
        $("#report_generator_spinner").hide();
        // Start checking for pending reports.
        $.cookie(PENDING_REPORT_CHECK_ID, setInterval(checkPendingReports, 1000));
    }

    setupReportForm();
    initializeReportFilesTable();

    $("#report_graph_source_id").change(function() {
        initiateGraphCommandDropDownUpdate();
    });

    $('#additional_report_attributes_toggle').click(function() {
        if (!$('#additional_report_attributes').is(":visible") ) {
            $('#additional_report_attributes_toggle').attr('class', 'btn btn-default btn-xs fa fa-angle-up');
        }

        else {
            $('#additional_report_attributes_toggle').attr('class', 'btn btn-default btn-xs fa fa-angle-down');
        }

        $('#additional_report_attributes').slideToggle();

        initializeServersTable();
        initializeServerGroupsTable();
    });
});

function setupReportForm() {
    processReportForm();

    $('#' + SCHEDULE_REPORT_RUN_BOX).click(function () {
        processReportForm();
    });

    $("#report_graph_source_id").change(function () {
        var $selected = $("#report_graph_source_id").val();

        if ($("#report_graph_source_id option[value='"+$selected+"']").text() == 'InfluxDB') {
            alert("The InfluxDB graph source is in alpha. It will not retrieve data for Windows based servers and is much slower than Nagios at the moment. It does how ever provide a better looking report and more upto date data. Use at your own risk.");
        }
    });

    $('#additionaloptions_toggle').click(function() {
        if (!$('#additionaloptions_items').is(":visible") ) {
            console.log("additionaloptions_items is visible");
            $('#additionaloptions_toggle').attr('class', 'btn btn-default btn-xs fa fa-angle-up');
        }

        else {
            console.log("additionaloptions_items is not visible");
            $('#additionaloptions_toggle').attr('class', 'btn btn-default btn-xs fa fa-angle-down');
        }

        $('#additionaloptions_items').slideToggle();
    });

    $('#generate_report_link').click(function (event) {
        $('#generate_report_form').find('#report_generation_information_email').val("");
        $('#generate_report_form').slideToggle();
    });

    $('#generate_report_form').find('#action_button').click(function (event) {
        $('#generate_report_form').slideToggle();
    });

    $("#report_include_application_information").click(function (event) {
        $('#report_show_all_applications').prop("disabled", !($('#report_include_application_information')[0].checked));
    });
}

function processReportForm() {
    if ($('#' + SCHEDULE_REPORT_RUN_BOX).is(':checked')) {
        $('#' + REPORT_SCHEDULE_DIV).children('div').children('select').prop('disabled', false);
    }

    else {
        $('#' + REPORT_SCHEDULE_DIV).children('div').children('select').prop('disabled', true);
    }

}

// Add a report to monitor
function addPendingReport(job_id, report_file_id, report_name, report_id) {
    // Get the all of the pending reports.
    var reports = getPendingJobs();

    // If the report with the passed in job id
    //  is already added, then throw an error
    //  indicating it has been.
    if (reports[job_id]) {
        displayAlert("Duplicate generation of report file <a href='/report_files/" + report_file_id + "'>" + report_name + "<\/a> with job id: "  + job_id + " has not been queued");
    }

    // If the report with the passed in job id
    //  doesn't exists add it to the reports
    //  array.
    else {
        reports[job_id] = {};
        reports[job_id]["report_id"] = report_id;
    }

    // Update the PENDING_REPORTS cookie and serialize the reports
    //  array.
    savePendingJobs(reports);
}

// Remove the report with the passed in job id
function removePendingReport(job_id) {
    // Get the currently pending reports
    var reports = getPendingJobs();

    console.log(reports[job_id]);
    // Delete the report with the passed in job id.
    delete reports[job_id];

    // Save reports to the pending reports cookie.
    savePendingJobs(reports);
}

// Remove the report with the passed in job id
function removeWorkingReport(job_id) {
    // Get the currently pending reports
    var reports = getWorkingJobs();

    console.log(reports[job_id]);
    // Delete the report with the passed in job id.
    delete reports[job_id];

    // Save reports to the pending reports cookie.
    saveWorkingJobs(reports);


    // Get the currently pending reports
    reports = getWorkingJobs();

    if (typeof Object.keys(reports).length === 'undefined' || (Object.keys(reports).length == 0)) {
        $("#report_generator_spinner").hide();
    }
}

// Serialize the passed in report to the PENDING_REPORTS cookie.
function savePendingJob(report) {
    console.log("Saving Jobs to Cookie");
    console.log(reports);
    //console.log("Called by: " + arguments.callee.caller.toString());
    var reports = getWorkingJobs();
    reports[report.job_id] = report;
    $.cookie(PENDING_REPORTS, JSON.stringify(reports), {path: '/'});
}

// Serialize the passed in reports to the PENDING_REPORTS cookie.
function savePendingJobs(reports) {
    console.log("Saving Jobs to Cookie");
    console.log(reports);
    //console.log("Called by: " + arguments.callee.caller.toString());
    $.cookie(PENDING_REPORTS, JSON.stringify(reports), {path: '/'});
}

// Serialize the passed in report to the PENDING_REPORTS cookie.
function saveWorkingJob(report) {
    console.log("Saving Jobs to Cookie");
    console.log(reports);
    //console.log("Called by: " + arguments.callee.caller.toString());
    var reports = getWorkingJobs();
    reports[report.job_id] = report;
    $.cookie(WORKING_REPORTS, JSON.stringify(reports), {path: '/'});
}

// Serialize the passed in reports to the PENDING_REPORTS cookie.
function saveWorkingJobs(reports) {
    console.log("Saving Jobs to Cookie");
    console.log(reports);
    //console.log("Called by: " + arguments.callee.caller.toString());
    $.cookie(WORKING_REPORTS, JSON.stringify(reports), {path: '/'});
}

// Get the pending reports.
function getPendingJobs() {
    // If the PENDING_REPORTS cookie is not defined.
    if (typeof $.cookie(PENDING_REPORTS) === 'undefined') {
        return new Object();
    }

    // Else parse the PENDING_REPORTS cookie, and return reports.
    else {
        var reports = JSON.parse($.cookie(PENDING_REPORTS));

        return reports;
    }
}

// Get the pending reports.
function getWorkingJobs() {
    // If the PENDING_REPORTS cookie is not defined.
    if (typeof $.cookie(WORKING_REPORTS) === 'undefined') {
        return new Object();
    }

    // Else parse the PENDING_REPORTS cookie, and return reports.
    else {
        var reports = JSON.parse($.cookie(WORKING_REPORTS));

        return reports;
    }
}

// Initiate a status check of all of the pending reports.
function checkPendingReports() {
    //console.log("Checking for pending reports");
    // Get the pending reports.
    var reports = getPendingJobs();
    //console.log(reports);

    // If their are pending reports
    if (typeof Object.keys(reports).length !== 'undefined' || (Object.keys(reports).length > 0)) {
        //console.log(Object.keys(reports).length + " reports still processing.");

        // Process each of the reports
        for (var job_id in reports) {
           // console.log("Processing: " + job_id);
            var report = reports[job_id];
            //console.log("Report status: " + report.status);
            if (report.status == "complete" || report.status == "failed") {
                //console.log("Report processing complete, skipping...");
                continue;
            }

            report.job_id = job_id;

            if(!statusStatusCheck(report)){
                startStatusCheck(report);
            }

            removePendingReport(report.job_id);
        }
    }

    // If there are no more reports being process.
    else {
        // Indicate that the STATUS_CHECK should be stopped.
        //stopStatusCheck(report);

        // Hide the report generator spinner.
        $("#report_generator_spinner").hide();
    }
}

function startStatusCheck(report) {
    var currently_running = statusStatusCheck(report);
    console.log("Status check for " + report.job_id + " is running? " + currently_running);
    if (!currently_running) {
        console.log("Starting status check for " + report.job_id);
        var id = setInterval(function() { statusCheck(report); }, 5000);

        report.status_check_id = id;

        savePendingJob(report);
    }
}

function statusStatusCheck(report) {
    var id = report.status_check_id;
    var status = false;

    if (id > -1) {
        status = true;
    }

    return status;
}

function stopStatusCheck(report) {
    if (statusStatusCheck(report)) {
        var id = report.status_check_id;
        clearInterval(id);
    }
}

// Check the status of the report associated with the passed in data.
function statusCheck(report) {
    console.log("Status Check Method");
    console.log(report);

    // Show the report generate spinner
    $("#report_generator_spinner").show();

    // The API URL that should be used to check the status of the report.
    var API_URL = "/api/reports/" + report.report_id + "/generate_status?job_id=" + report.job_id;

    // Initiate the status check.
    var jqXHR = jQuery.ajax({
        url:    API_URL,
        async:   false
    });
    var data = jqXHR.responseJSON

    // Initialize the boolean that indicates whether pending reports should be updated.
    var save_status = false;

    var reports = getWorkingJobs();

    // If the current report is not valid, remove it.
    if (typeof report === 'undefined') {
        // Remove the current report.
        removeWorkingReport(data.job_id);
        // Stop further processing
        return;
    }


    // If the current_report does not contain a valid report_file_id,
    //  update it with the one from the passed in data variable.
    if (report.report_file_id == null) {
        // Set the report_file_id of the current report with the one from the passed in data variable.
        report.report_file_id = data.report_file_id;
        reports[report.job_id] = report;

        saveWorkingJobs(reports);
    }

    switch (data.status) {
        case 'queued':
            if (report.status != data.status) {
                displayNotice("Report <a href='/report_files/" + data.report_file_id + "'>" + data.report_name + "<\/a> with job id: "  + data.job_id + " has been queued.");
                save_status = true;
            }
            break;
        case 'working':
            if (report.status != data.status) {
                displayNotice("Report <a href='/report_files/" + data.report_file_id + "'>" + data.report_name + "<\/a> with job id: "  + data.job_id + " is being processed.");
                save_status = true;
            }
            break;
        case 'complete':
            displayNotice("Report <a href='/report_files/" + data.report_file_id + "'>" + data.report_name + "<\/a> complete.");
            removeWorkingReport(report.job_id);
            stopStatusCheck(report);

            updateTable(report);
            break;
        case 'failed':
            displayAlert("Report generation of <a href='/report_files/" + data.report_file_id + "'>" + data.report_name + "<\/a> with job id: "  + data.job_id + " has failed.");
            removeWorkingReport(report.job_id);
            stopStatusCheck(report);
            break;
        case null:
            removeWorkingReport(report.job_id);
            stopStatusCheck(report);
            break;
    }

    console.log("Report " + data.status);
    if (save_status) {
        console.log("Save Status: " + save_status);
        console.log(report);
        console.log(data);
        report["status"] = data.status;
        reports[data.job_id] = report;
        saveWorkingJobs(reports);
    }
}

function updateTable(report) {
    console.log("Updating tables...");
    console.log(report);
    $.getJSON( "/api/report_files/" + report.report_file_id, function(report_file) {
        update_report_files_table(report_file)
    });
}

function initiateGraphCommandDropDownUpdate() {
    var graph_source = $("#report_graph_source_id").find(":selected").val();

    var api_url = "/api/graph_sources/" + graph_source +  "/graph_commands";

    var jqxhr = $.get( api_url, function(data) {
        $("#report_graph_command_id").children().remove();
        updateGraphCommandDropDown(data);

    })
        .fail(function() {
            alert( "error" );
        });
}

function updateGraphCommandDropDown(graphCommands) {
    for (var i = 0; i < graphCommands.length; i++) {
        var optionStr = '<option value="' + graphCommands[i].id + '">' + graphCommands[i].full_name +'</option>';

        $("#report_graph_command_id").append(optionStr);
    }
}