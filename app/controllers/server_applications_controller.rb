class ServerApplicationsController < ApplicationController
  # GET /server_applications
  # GET /server_applications.json
  def index
    @server_applications = ServerApplication.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @server_applications }
    end
  end

  # GET /server_applications/1
  # GET /server_applications/1.json
  def show
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @server_application }
    end
  end

  # GET /server_applications/new
  # GET /server_applications/new.json
  def new
    @server_application = ServerApplication.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @server_application }
    end
  end

  # GET /server_applications/1/edit
  def edit
    @server_application = ServerApplication.find(params[:id])
  end

  # POST /server_applications
  # POST /server_applications.json
  def create
    @server_application = ServerApplication.new(server_application_params)

    respond_to do |format|
      if @server_application.save
        format.html { redirect_to @server_application, notice: 'Server application was successfully created.' }
        format.json { render json: @server_application, status: :created, location: @server_application }
      else
        format.html { render action: "new" }
        format.json { render json: @server_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /server_applications/1
  # PATCH/PUT /server_applications/1.json
  def update
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      if @server_application.update_attributes(server_application_params)
        format.html { redirect_to @server_application, notice: 'Server application was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @server_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /server_applications/1
  # DELETE /server_applications/1.json
  def destroy
    @server_application = ServerApplication.find(params[:id])
    @server_application.destroy

    respond_to do |format|
      format.html { redirect_to server_applications_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def server_application_params
      params.require(:server_application).permit(:active, :application_id, :description, :ignore_active, :server_id)
    end
end
