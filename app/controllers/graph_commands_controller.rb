class GraphCommandsController < ApplicationController
  load_and_authorize_resource

  # GET /graph_commands
  # GET /graph_commands.json
  def index
    @graph_commands = GraphCommand.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @graph_commands }
    end
  end

  # GET /graph_commands/1
  # GET /graph_commands/1.json
  def show
    @graph_command = GraphCommand.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @graph_command }
    end
  end

  # GET /graph_commands/new
  # GET /graph_commands/new.json
  def new
    @graph_command = GraphCommand.new
    @graph_command.user = current_user

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @graph_command }
    end
  end

  # GET /graph_commands/1/edit
  def edit
    @graph_command = GraphCommand.find(params[:id])
  end

  # POST /graph_commands
  # POST /graph_commands.json
  def create
    @graph_command = GraphCommand.new(params[:graph_command])

    respond_to do |format|
      if @graph_command.save
        format.html { redirect_to @graph_command, notice: 'Graph command was successfully created.' }
        format.json { render json: @graph_command, status: :created, location: @graph_command }
      else
        format.html { render action: "new" }
        format.json { render json: @graph_command.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /graph_commands/1
  # PUT /graph_commands/1.json
  def update
    @graph_command = GraphCommand.find(params[:id])

    respond_to do |format|
      if @graph_command.update_attributes(params[:graph_command])
        format.html { redirect_to @graph_command, notice: 'Graph command was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @graph_command.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /graph_commands/1
  # DELETE /graph_commands/1.json
  def destroy
    @graph_command = GraphCommand.find(params[:id])
    @graph_command.destroy

    respond_to do |format|
      format.html { redirect_to graph_commands_url }
      format.json { head :no_content }
    end
  end
end
