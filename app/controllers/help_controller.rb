class HelpController < ApplicationController
  def about
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def download_manual
    send_file Rails.root.join('doc', 'PerfReport.docx'), :type=> 'application/zip', :x_sendfile=>true
  end
end
