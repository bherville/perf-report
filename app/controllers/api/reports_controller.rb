class API::ReportsController < ApplicationController
  load_and_authorize_resource
  before_filter :owner_check_read, only: [:generate, :generate_status]

  def generate
    report = Report.find(params[:id])
    timestamp = Time.now.in_time_zone(current_user.time_zone)
    reports_dir = Rails.root.join('public', 'generated_reports')
    Dir.mkdir reports_dir unless Dir.exists?(reports_dir)

    report_name = "#{timestamp.strftime('%m-%d-%Y_%H_%M_%S')}_#{report.name}.pdf"


    email_address = params[:email_address].empty? ? nil : params[:email_address]
    job = ReportGenerator.perform_async(params[:id], report_name, current_user.id, email_address)

    respond_to do |format|
      format.json do
        render json: {
            :status => 'queued',
            :job_id => job,
            :report_id => report.id,
            :report_pdf => "/generated_reports/#{report_name}.pdf"
        }.to_json
      end
    end
  end

  def generate_status
    report = Report.find(params[:id])
    job_id = params[:job_id]
    job_status = Sidekiq::Status::get_all (job_id)
    report_file = report.report_files.find {|s| s.job_id == job_id }
    report_pdf = report_file.nil? ? nil : "/generated_reports/#{report_file.file_file_name}"

    respond_to do |format|
      format.json do
        render json: {
            :status         => job_status['status'],
            :updated_time   => job_status['update_time'],
            :job_id         => job_id,
            :report_id      => report.id,
            :report_name    => report.name,
            :report_pdf     => report_pdf,
            :report_file_id => report_file.nil? ? nil : report_file.id
        }.to_json
      end
    end
  end

  def user_report_files
    report = Report.find(params[:id])

    @report_files = report.report_files.find_all {|f| f.user == current_user }
  end
end
