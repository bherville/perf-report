class API::GraphCommandsController < ApplicationController
  load_and_authorize_resource

  # GET /graph_commands.json
  def index
    @graph_commands = GraphSource.find(params[:graph_source_id]).graph_commands

    respond_to do |format|
      format.json
    end
  end

  # GET /graph_commands/1.json
  def show
    @graph_command = GraphCommand.find(params[:id])

    respond_to do |format|
      format.json
    end
  end
end
