class API::ReportFilesController < ApplicationController
  def index
    report_files = ReportFile.all

    respond_to do |format|
      format.json do
        render json: report_files.to_json
      end
    end
  end

  def show
    @report_file = ReportFile.find(params[:id])
  end
end
