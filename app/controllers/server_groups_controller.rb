class ServerGroupsController < ApplicationController
  load_and_authorize_resource

  # GET /server_groups
  # GET /server_groups.json
  def index
    @server_groups = ServerGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @server_groups }
    end
  end

  # GET /server_groups/1
  # GET /server_groups/1.json
  def show
    @server_group = ServerGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @server_group }
    end
  end

  # GET /server_groups/new
  # GET /server_groups/new.json
  def new
    @server_group = ServerGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @server_group }
    end
  end

  # GET /server_groups/1/edit
  def edit
    @server_group = ServerGroup.find(params[:id])
  end

  # POST /server_groups
  # POST /server_groups.json
  def create
    @server_group = ServerGroup.new(params[:server_group])
    @server_group.user = current_user

    respond_to do |format|
      if @server_group.save
        format.html { redirect_to @server_group, notice: 'Server group was successfully created.' }
        format.json { render json: @server_group, status: :created, location: @server_group }
      else
        format.html { render action: "new" }
        format.json { render json: @server_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /server_groups/1
  # PUT /server_groups/1.json
  def update
    @server_group = ServerGroup.find(params[:id])

    respond_to do |format|
      if @server_group.update_attributes(params[:server_group])
        format.html { redirect_to @server_group, notice: 'Server group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @server_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /server_groups/1
  # DELETE /server_groups/1.json
  def destroy
    @server_group = ServerGroup.find(params[:id])
    @server_group.destroy

    respond_to do |format|
      format.html { redirect_to server_groups_url }
      format.json { head :no_content }
    end
  end
end
