class Admin::UsersController < ApplicationController
  load_and_authorize_resource

  def index
    @users = User.all.select{ |user| user.email != SYSTEM_USER_ACCOUNT_EMAIL }
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
    @roles = User.valid_roles
    @user.build_user_profile
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = t('admin.users.created_user')
      redirect_to admin_users_path
    else
      render :action => 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    @roles = @user.roles
    @user.build_user_profile if @user.user_profile.nil?
  end

  def update
    @user = User.find(params[:id])
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?

    params[:user][:roles] = Array.new if params[:user][:roles].nil?

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_user_path, notice: t('admin.users.updated_user') }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    if current_user == @user
      flash[:alert] = t('admin.users.cannot_delete')
    elsif @user.destroy
      flash[:notice] = t('admin.users.deleted_user')
    end

    respond_to do |format|
      format.html { redirect_to admin_users_path }
      format.json { head :no_content }
    end
  end
end