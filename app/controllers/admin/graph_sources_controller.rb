class Admin::GraphSourcesController < ApplicationController
  load_and_authorize_resource

  # GET /graph_sources
  def index
    @graph_sources = GraphSource.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /graph_sources/1
  def show
    @graph_source = GraphSource.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /graph_sources/new
  def new
    @graph_source = GraphSource.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /graph_sources/1/edit
  def edit
    @graph_source = GraphSource.find(params[:id])
  end

  # POST /graph_sources
  def create
    @graph_source = GraphSource.new(params[:graph_source])
    @graph_source.user = current_user

    respond_to do |format|
      if @graph_source.save
        format.html { redirect_to admin_graph_source_path(@graph_source), notice: 'Graph source was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PATCH/PUT /graph_sources/1
  def update
    @graph_source = GraphSource.find(params[:id])
    @graph_source.user = current_user

    respond_to do |format|
      if @graph_source.update_attributes(params[:graph_source])
        format.html { redirect_to admin_graph_source_path(@graph_source), notice: 'Graph source was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /graph_sources/1
  def destroy
    @graph_source = GraphSource.find(params[:id])
    @graph_source.destroy

    respond_to do |format|
      format.html { redirect_to admin_graph_sources_path }
    end
  end
end
