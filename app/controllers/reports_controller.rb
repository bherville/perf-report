class ReportsController < ApplicationController
  load_and_authorize_resource
  before_filter :owner_check_modify, only: [:edit, :update, :destroy]
  before_filter :owner_check_read, only: [:show, :generate]


  # GET /reports
  def index
    @reports = Report.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /reports/1
  def show
    @report = Report.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.pdf do
        pdf_hash = @report.setup_pdf(params[:debug])

        render pdf: @report.name,
               show_as_html: params[:debug],
               :locals => {
                 :report    => @report,
                 :graphs    => pdf_hash[:graphs],
                 :app_info  => pdf_hash[:app_info]
               }
      end
    end
  end

  # GET /reports/new
  def new
    @report = Report.new
    @user = current_user
    @users = User.order(:email).all(:conditions => ['id != ?', @user.id]).select { |user| user.user_profile.allow_shared_reports? }
    setup_graph_types

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /reports/1/edit
  def edit
    @report = Report.find(params[:id])
    @user = current_user
    @users = User.order(:email).all(:conditions => ['id != ?', @user.id]).select { |user| user.user_profile.allow_shared_reports? }
    setup_graph_types
  end

  # POST /reports
  def create
    @report = Report.new(params[:report])
    @report.user = current_user
    @user = current_user
    @users = User.order(:email).all(:conditions => ['id != ?', @report.user.id]).select { |user| user.user_profile.allow_shared_reports? }

    respond_to do |format|
      if @report.save
        format.html { redirect_to @report, notice: 'Report was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /reports/1
  def update
    @report = Report.find(params[:id])
    @user = current_user
    @users = User.order(:email).all(:conditions => ['id != ?', @report.user.id]).select { |user| user.user_profile.allow_shared_reports? }

    respond_to do |format|
      if @report.update_attributes(params[:report])
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /reports/1
  def destroy
    @report = Report.find(params[:id])
    @report.destroy

    respond_to do |format|
      format.html { redirect_to reports_url }
    end
  end

  def generate
    @report = Report.find(params[:id])

    @email_address = params[:report_generation_information][:email]

    respond_to do |format|
      format.js
    end
  end


  private
  def setup_graph_types
    @report.graph_types ||= Array.new
  end

  def owner_check_modify
    report = Report.find(params[:id])
    unless report.can_modify?(current_user)
      params[:redirect_to] = reports_path
      raise CanCan::AccessDenied
    end
  end
end
