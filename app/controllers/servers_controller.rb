class ServersController < ApplicationController
  load_and_authorize_resource

  # GET /servers
  def index
    @servers = Server.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /servers/1
  def show
    @server = Server.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # DELETE /servers/1
  def destroy
    @server = Server.find(params[:id])
    @server.destroy

    respond_to do |format|
      format.html { redirect_to servers_url }
      format.json { head :no_content }
    end
  end
end
