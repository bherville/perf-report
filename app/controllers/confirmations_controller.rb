class ConfirmationsController < Devise::ConfirmationsController
  def show
    @user = User.find_by_confirmation_token(params[:confirmation_token]) if params[:confirmation_token].present?

    super if @user.nil? or @user.confirmed?
  end

  def confirm
    confirm = params[:user][:confirmation_token]
    @user = User.find_by_confirmation_token(confirm)
    @user.build_user_profile

    if @user.update_attributes(params[:user].except(:confirmation_token)) && @user.password_match?
      @user.confirm!
      set_flash_message :notice, :confirmed
      sign_in_and_redirect(resource_name, resource)
    else
      render :action => 'show'
    end
  end
end