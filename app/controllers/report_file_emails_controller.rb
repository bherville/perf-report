class ReportFileEmailsController < ApplicationController
  load_and_authorize_resource

  # GET /reports_files/1/email
  def new
    @report_file = ReportFile.find(params[:report_file_id])
    @report_file_email = @report_file.report_file_emails.new(:report_file_id => @report_file.id)

    respond_to do |format|
      if @report_file.file_exists?
        format.html
      else
        format.html {
          redirect_to @report_file, :alert => 'The file associated with this ReportFile no longer exists. Not sending email...'
        }
      end
    end
  end

  def create
    @report_file_email = ReportFileEmail.create(params[:report_file_email])
    @report_file_email.report_file_id = params[:report_file_id]

    respond_to do |format|
      if @report_file_email.save
        format.html {
          redirect_to :action => :send_email, :report_file_email_id => @report_file_email
        }
      else
        format.html { render "new" }
      end
    end
  end

  # GET /reports_files/1/send_email
  def send_email
    report_file_email = ReportFileEmail.find(params[:report_file_email_id])

    respond_to do |format|
      if report_file_email.report_file.file_exists?
        format.html {

          ReportMailer.delay.pdf_email(report_file_email)

          redirect_to report_file_email.report_file, :notice => 'Your email has been queued to be sent.'
        }
      else
        format.html {
          redirect_to report_file_email.report_file, :alert => 'The file associated with this ReportFile no longer exists. Not sending email...'
        }
      end
    end
  end
end
