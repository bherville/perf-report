class GraphTypesController < ApplicationController
  load_and_authorize_resource

  # GET /graph_types
  # GET /graph_types.json
  def index
    @graph_types = GraphType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @graph_types }
    end
  end

  # GET /graph_types/1
  # GET /graph_types/1.json
  def show
    @graph_type = GraphType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @graph_type }
    end
  end

  # GET /graph_types/new
  # GET /graph_types/new.json
  def new
    @graph_type = GraphType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @graph_type }
    end
  end

  # GET /graph_types/1/edit
  def edit
    @graph_type = GraphType.find(params[:id])
  end

  # POST /graph_types
  # POST /graph_types.json
  def create
    @graph_type = GraphType.new(params[:graph_type])
    @graph_type.user = current_user

    respond_to do |format|
      if @graph_type.save
        format.html { redirect_to @graph_type, notice: 'Graph type was successfully created.' }
        format.json { render json: @graph_type, status: :created, location: @graph_type }
      else
        format.html { render action: "new" }
        format.json { render json: @graph_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /graph_types/1
  # PUT /graph_types/1.json
  def update
    @graph_type = GraphType.find(params[:id])

    respond_to do |format|
      if @graph_type.update_attributes(params[:graph_type])
        format.html { redirect_to @graph_type, notice: 'Graph type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @graph_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /graph_types/1
  # DELETE /graph_types/1.json
  def destroy
    @graph_type = GraphType.find(params[:id])
    @graph_type.destroy

    respond_to do |format|
      format.html { redirect_to graph_types_url }
      format.json { head :no_content }
    end
  end
end
