class ReportFilesController < ApplicationController
  load_and_authorize_resource

  # GET /reports_files
  def index
    @report_files = ReportFile.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /reports_files/1
  def show
    @report_file = ReportFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end
end
