class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_timezone

  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      flash[:alert] = t('user.not_authorized')
      session[:user_return_to] = nil

      if params[:redirect_to]
        redirect_path = params[:redirect_to]
      else
        redirect_path = (current_user.roles.count == 2 && current_user.has_role?(:admin)) ? admin_path : root_path
      end

      respond_to do |format|
        format.html { redirect_to redirect_path }
        format.json { render json: { :message => flash[:error], :url => request.url } }
      end

    else
      flash[:alert] = t('user.login_first')
      session[:user_return_to] = request.url

      respond_to do |format|
        format.html { redirect_to '/users/sign_in' }
        format.json { render json: { :message => flash[:error], :url => request.url } }
      end
    end
  end

  def set_timezone
    Time.zone = current_user.time_zone if current_user
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def after_sign_in_path_for(resource)
    if resource.email == SYSTEM_USER_ACCOUNT_EMAIL
      destroy_user_session_path
      raise CanCan::AccessDenied
    elsif resource.has_role?(:admin) && resource.roles.count == 2
      admin_path
    else
      root_path
    end
  end

  def owner_check_read
    report = Report.find(params[:id])
    unless report.can_read?(current_user)
      params[:redirect_to] = reports_path
      raise CanCan::AccessDenied
    end
  end
end