class GraphsController < ApplicationController
  load_and_authorize_resource

  # GET /graphs
  def index
    @graphs = Graph.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /graphs/1
  def show
    @graph = Graph.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # DELETE /graphs/1
  def destroy
    @graph = Graph.find(params[:id])
    @graph.destroy

    respond_to do |format|
      format.html { redirect_to graphs_url }
    end
  end
end
