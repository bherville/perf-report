require 'clockwork'
require '../config/boot'
require '../config/environment'

module Clockwork
  handler do |job|
    puts "Running #{job}"
  end


  if Rails.env == 'development'
    check_every = 1.minute
  else
    check_every = 5.minutes
  end
  every(check_every, 'report_file.send')  {
    reports = Report.all

    reports.each do |report|
      unless report.schedule_report_run
        puts "Report #{report.id} has not been requested to be ran automatically. Skipping..."
        next
      end

      time = Time.zone.now
      day = time.strftime("%A")
      puts "Processing report with id: #{report.id}."

      if report.send_time.strftime( "%H%M%S%N" ) <= time.strftime( "%H%M%S%N" ) && (report.send_day.downcase == day.downcase || report.send_day.downcase.to_sym == :all)
        puts "Report send time #{report.send_time.strftime( "%H%M%S%N" )} is less then or equal to current time #{time.strftime( "%H%M%S%N" )} and it is the same day #{report.send_day} == #{day.downcase} or its set to all. Will process further..."
        if report.last_time_sent.nil? || ((Date.parse(report.last_time_sent.to_s) < Date.parse(time.to_s) || report.send_day.downcase.to_sym == :all) && report.last_time_sent.to_date < time.to_date)
          puts "Report #{report.id} last_time_sent is not set or is less then today's date. Will generate and send..."
          timestamp = Time.zone.now

          report_name = "#{timestamp.strftime('%m-%d-%Y_%H_%M_%S')}_#{report.name}.pdf"

          ReportGeneratorMail.perform_async(report.id, report_name, !report.has_mailing_list?)

          puts "Setting last_time_sent for report #{report.id} to #{time}"

          report.last_time_sent = time
          report.save!
        else
          puts "Report #{report.id} last_time_sent is set and is equal to or greater then today's date. Skipping..."
        end

      else
        puts "Report #{report.id} schedule threshold has not yet been reached. Skipping..."
      end
    end
  }
end